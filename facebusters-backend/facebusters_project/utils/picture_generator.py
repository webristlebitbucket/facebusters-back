from django.conf import settings
from time import time
# from pyvirtualdisplay import Display
from selenium import webdriver


def generate_screenshot():
    '''
    # Create virtual display
    display = Display(visible=0, size=(800, 800))
    display.start()
    
    WEBDRIVER = settings.WEBDRIVER

    # Start Web Driver
    driver = webdriver.Firefox(executable_path=WEBDRIVER)
    driver.get("http://localhost/static/demo/test.html")

    # Get Picture binary
    screenshot = driver.get_screenshot_as_png()

    driver.quit() # Quit the driver and close every associated window.
    display.stop() # Quit the screen.
    return screenshot
    '''

    WEBDRIVER = settings.WEBDRIVER

    # Start Web Driver
    driver = webdriver.PhantomJS(executable_path=r'/usr/bin/phantomjs')
    driver.set_window_size(800, 800)
    driver.get("http://localhost/static/demo/test.html")

    # Get Picture binary
    screenshot = driver.get_screenshot_as_png()

    driver.quit() # Quit the driver and close every associated window.
    return screenshot
    