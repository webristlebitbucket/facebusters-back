from django import template

register = template.Library()


@register.filter(name='percentage')
def percentage(value, arg):
    return (value / arg) * 100
