from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from baton.autodiscover import admin
from django.views.generic import TemplateView
from django.views import defaults as default_views
from ajax_select import urls as ajax_select_urls
from facebusters_project.pictures.views import picture_detail, picture_social

urlpatterns = [
    # Django Admin, use {% url 'admin:index' %}
    #url(r'^$', index, name='index'),
    url(r'^$', include('facebusters_project.api.v1.router')),

    url(r'^api', include('facebusters_project.api.v1.router')),
    url(r'^ajax_select/', include(ajax_select_urls)),
    url(settings.ADMIN_URL, admin.site.urls),
    url(r'^baton/', include('baton.urls')),
    url(r'^api/v1/', include('facebusters_project.api.v1.router')),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^picture/(?P<identifier>[a-zA-Z0-9\-]+)$',
        picture_detail, name='picture_detail'),
    url(r'^picture_social/(?P<identifier>[a-zA-Z0-9\-]+)$',
        picture_social, name='picture_social')

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', default_views.bad_request,
            kwargs={'exception': Exception('Bad Request!')}),
        url(r'^403/$', default_views.permission_denied,
            kwargs={'exception': Exception('Permission Denied')}),
        url(r'^404/$', default_views.page_not_found,
            kwargs={'exception': Exception('Page not Found')}),
        url(r'^500/$', default_views.server_error),
    ]
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar
        urlpatterns = [
            url(r'^__debug__/', include(debug_toolbar.urls)),
        ] + urlpatterns
