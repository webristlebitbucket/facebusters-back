# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-12-12 17:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notifications', '0003_auto_20181104_1928'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notificationlibrary',
            name='notification_type',
            field=models.CharField(blank=True, choices=[('generic', 'Generic'), ('picture', 'Picture'), ('place', 'Place'), ('alert', 'Alert'), ('user', 'User')], max_length=5000, null=True, verbose_name='Notification Type'),
        ),
    ]
