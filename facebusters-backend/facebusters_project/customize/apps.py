from django.apps import AppConfig


class CustomizeConfig(AppConfig):
    name = 'facebusters_project.customize'
    verbose_name = "Customize"
