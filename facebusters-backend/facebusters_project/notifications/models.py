from django.db import models
from django.utils import timezone
import uuid
from .managers import ActiveNotificationsManager
from django.db.models.signals import post_save

class NotificationLibrary(models.Model):
    ViewChoices = (
        ('timeline', 'Timeline'),
        ('profile', 'Profile'),
        ('public-profile', 'Public profile'),
        ('notification-list', 'Notifications list'),
    )
    TypeChoices = (
        ('generic', 'Generic'),
        ('picture', 'Picture'),
        ('place', 'Place'),
        ('alert', 'Alert'),
        ('user', 'User'),

    )
    id = models.AutoField(
        primary_key=True
    )
    key = models.CharField(
        verbose_name="Clave",
        max_length=500,
        db_index=True
    )
    notification_type = models.CharField(
        verbose_name="Notification Type",
        null=True,
        blank=True,
        choices=TypeChoices,
        max_length=5000
    )
    text_en = models.CharField(
        verbose_name="Text EN",
        null=True,
        blank=True,
        max_length=5000
    )
    text_es = models.CharField(
        verbose_name="Text ES",
        null=True,
        blank=True,
        max_length=5000
    )
    text_it = models.CharField(
        verbose_name="Text IT",
        null=True,
        blank=True,
        max_length=5000
    )
    text_fr = models.CharField(
        verbose_name="Text FR",
        null=True,
        blank=True,
        max_length=5000
    )

    active = models.BooleanField(
        verbose_name="Active",
    )
    push = models.BooleanField(
        verbose_name="Push",
        default=False
    )
    email = models.BooleanField(
        verbose_name="Email",
        default=False
    )
    linked_view = models.CharField(
        verbose_name="Linked view",
        null=True,
        blank=True,
        choices=ViewChoices,
        max_length=5000
    )

    created_at = models.DateTimeField(
        editable=False
    )
    updated_at = models.DateTimeField(
        editable=False
    )
    objects = models.Manager()
    active_notifications = ActiveNotificationsManager()

    def __str__(self):
        return str(self.key)

    class Meta:
        verbose_name = u'Notifications'
        verbose_name_plural = u'Notifications'
        ordering = ['created_at', ]

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        return super(NotificationLibrary, self).save(*args, **kwargs)


class NotificationItem(models.Model):
    StatusChoices = (
        ('readed', 'Readed'),
        ('deleted', 'Deleted'),
        ('received', 'Recived')
    )
    id = models.AutoField(
        primary_key=True
    )
    user = models.ForeignKey(
        'users.User',
        related_name='NotificationItem_to_User',
        verbose_name=u'User'
    )
    notification_library = models.ForeignKey(
        NotificationLibrary,
        related_name='NotificationItem_to_NotificationLibrary',
        verbose_name=u'Notification'
    )
    status = models.CharField(
        verbose_name='Status',
        max_length=500,
        null=True,
        blank=False,
        choices=StatusChoices
    )
    related_item_key = models.CharField(
        verbose_name="Related item key",
        max_length=300,
        blank=True,
        null=True
    )
    created_at = models.DateTimeField(
        editable=False
    )
    updated_at = models.DateTimeField(
        editable=False
    )

    def __str__(self):
        return str(self.user.username) + ' ' + str(self.notification_library.key)

    class Meta:
        verbose_name = u'Notification of user'
        verbose_name_plural = u'Notification of user'
        ordering = ['created_at', ]

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        return super(NotificationItem, self).save(*args, **kwargs)


class UserDevices(models.Model):
    '''
        This model store devices linked to users. Active or not. 
    '''
    id = models.AutoField(
        primary_key=True
    )
    user = models.ForeignKey(
        'users.User',
        related_name='Device_to_User',
        verbose_name=u'User'
    )
    player_id = models.CharField(
        verbose_name='Player ID',
        max_length=500,
        null=True,
        blank=False,
    )
    active = models.BooleanField(
        verbose_name="Active",
        default=True
    )
    created_at = models.DateTimeField(
        editable=False
    )
    updated_at = models.DateTimeField(
        editable=False
    )

    def __str__(self):
        return str(self.user.username) + ' ' + str(self.player_id)

    class Meta:
        verbose_name = u'User Device'
        verbose_name_plural = u'User Devices'
        ordering = ['created_at', ]

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        return super(UserDevices, self).save(*args, **kwargs)


def save_notification(sender, instance, **kwargs):
    if instance.status == 'received':
        # We only send notification on create.
        from facebusters_project.notifications.utils import send_push_notification
        # Send PUSH
        if instance.notification_library.push == True:
            # Sample data 
            content = {}
            content['en'] = instance.notification_library.text_en
            content['es'] = instance.notification_library.text_es
            content['it'] = instance.notification_library.text_it
            content['fr'] = instance.notification_library.text_fr

            headings = {
                "en": "Facebusters", 
                "es": "Facebusters",
                "it": "Facebusters",
                "fr": "Facebusters",
            }

            # Get user devices
            available_devices = UserDevices.objects.filter(user=instance.user,active=True)


            all_devices = UserDevices.objects.all()
            print(all_devices)

            player_ids = []
            for device in available_devices:
                player_ids.append(device.player_id)  


            print(player_ids)
            # Send notification           
            push = send_push_notification (content=content,headings=headings,url='', player_ids=player_ids  )
        
    return True

post_save.connect(save_notification, sender=NotificationItem)