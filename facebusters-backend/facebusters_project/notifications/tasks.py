from background_task import background  # Importar Tarea
from consum_project.utils.code_responses import get_response
from django.apps import apps
from django.utils import timezone


@background(queue='notifications')  # Nombre de la tarea en queue
def send_notification(user_id, notificationlibrary_key):  # funcion a ejecutar
    User = apps.get_model(
        app_label='users', model_name='User')
    NotificationLibrary = apps.get_model(
        app_label='notifications', model_name='NotificationLibrary')
    NotificationItem = apps.get_model(
        app_label='notifications', model_name='NotificationItem')
    try:
        user = User.objects.get(pk=user_id)
        try:
            notification_library = NotificationLibrary.active_notifications.get(
                key=notificationlibrary_key)
            notification = NotificationItem()
            notification.user = user
            notification.notification_library = notification_library
            notification.status = 'received'
            notification.save()
            if user.active_notifications == True:
                '''
                # parte email
                if notification_library.email == True:
                    send_email(
                        user.email, notification_library.egoi_templatehash)
                # push notis
                if notification_library.push == True:
                    send_push(user.egoi_id, notification_library.egoi_tagid)
                '''
                return True
            else:
                return True
        except NotificationLibrary.DoesNotExist:
            response = get_response('NOTIFICATION_NOT_EXISTS')
            print (response['data'])
            return False
    except User.DoesNotExist:
        response = get_response('USER_NOT_EXISTS')
        print (response['data'])
        return False
