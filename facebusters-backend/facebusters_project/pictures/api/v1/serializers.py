from __future__ import unicode_literals
from rest_framework import serializers
from rest_framework.views import APIView
from facebusters_project.pictures.models import Picture, Album, Hashtag,UserComment, Usertag, MediaItem, ReportItem
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.authentication import TokenAuthentication
from utils.code_responses import get_response
from easy_thumbnails.files import get_thumbnailer
from django.contrib.gis.db import models as geomodels

# User imports
from facebusters_project.users.api.v1.serializers import UserMinSerializer
from facebusters_project.utils.utils import MultipartM2MField

# Extra Fields
from drf_extra_fields.geo_fields import PointField
from drf_extra_fields.fields import Base64ImageField


class PictureSerializer(serializers.ModelSerializer):
    image_thumbnail = serializers.SerializerMethodField('image_thumb')
    user_info = serializers.SerializerMethodField('user_data')
    image_tags = serializers.SerializerMethodField('tags_data')
    # hashtags = MultipartM2MField()
    image = Base64ImageField()
    meta_location = PointField()

    def image_thumb(self, obj):
        if obj.image_with_filter:
            image_source = obj.image_with_filter
        elif obj.image:
            image_source = obj.image
        else:
            return None

        if image_source is not None:
            image_url = get_thumbnailer(image_source)['stream_picture'].url
            if image_url is not None:
                try:
                    image_url = self.context.get("request").build_absolute_uri(image_url)
                except Exception as error:
                    image_url = None
            else:
                image_url = None
        else:
            image_url = None

        return image_url

    def user_data(self, obj):
        if obj.user:
            serializer = UserMinSerializer(
                obj.user, many=False, read_only=True,
                context={"request": self.context.get("request")})
            return serializer.data
        else:
            return None

    def tags_data(self, obj):
        tags = []
        for item in obj.hashtags.all():
            tags.append(item.name)
        return tags
 
    class Meta:
        model = Picture
        write_only_fields = ('user', )
        fields = ('identifier','user', 'image', 'image_thumbnail', 'title',
        'public','user_info','album', 'meta_location', 'meta_date',
        'address_es', 'address_it', 'address_fr', 'address_en', 'hide', 'order',
        'image_tags','hashtags','created_at', 'updated_at','picture_filter')


class SmallPictureSerializer(serializers.ModelSerializer):
    image_thumbnail = serializers.SerializerMethodField('image_thumb')
    meta_location = PointField()

    def image_thumb(self, obj):
        if obj.image_with_filter:
            image_source = obj.image_with_filter
        elif obj.image:
            image_source = obj.image
        else:
            return None

        if image_source is not None:
            image_url = get_thumbnailer(image_source)['stream_picture'].url
            if image_url is not None:
                try:
                    image_url = self.context.get("request").build_absolute_uri(image_url)
                except Exception as error:
                    image_url = None
            else:
                image_url = None
        else:
            image_url = None

        return image_url
 
    class Meta:
        model = Picture
        write_only_fields = ('user', )
        fields = ('identifier','image_thumbnail', 'meta_location', 'created_at', 'updated_at')


class CreatePictureSerializer(serializers.ModelSerializer):
    image_thumbnail = serializers.SerializerMethodField('image_thumb')
    user_info = serializers.SerializerMethodField('user_data')
    image_tags = serializers.SerializerMethodField('tags_data')
    image = Base64ImageField()
    meta_location = PointField()

    def image_thumb(self, obj):
        if obj.image_with_filter:
            image_source = obj.image_with_filter
        elif obj.image:
            image_source = obj.image
        else:
            return None

        if image_source is not None:
            image_url = get_thumbnailer(image_source)['stream_picture'].url
            if image_url is not None:
                try:
                    image_url = self.context.get("request").build_absolute_uri(image_url)
                except Exception as error:
                    image_url = None
            else:
                image_url = None
        else:
            image_url = None

        return image_url

    def user_data(self, obj):
        print('test')
        if obj.user:
            print('test2')
            serializer = UserMinSerializer(
                obj.user, many=False, read_only=True,
                context={"request": self.context.get("request")})
            return serializer.data
        else:
            return None


    def tags_data(self, obj):
        print(obj.hashtags.all())
        tags = []
        for item in obj.hashtags.all():
            tags.append(item.name)
        return tags

    class Meta:
        model = Picture
        write_only_fields = ('user', )

        fields = ('identifier','user', 'image', 'image_thumbnail', 'title',
        'public','user_info','album', 'meta_location', 'meta_date',
        'address_es', 'address_it', 'address_fr', 'address_en', 'hide', 'order',
        'image_tags','hashtags','created_at', 'updated_at','picture_filter', 'my_item')


class UserCommentSerializer(serializers.ModelSerializer):
    user_info = serializers.SerializerMethodField('user_data')

    def user_data(self, obj):
        if obj.user:
            serializer = UserMinSerializer(
                obj.user, many=False, read_only=True,
                context={"request": self.context.get("request")})
            return serializer.data
        else:
            return None

    class Meta:
        model = UserComment
        fields = ('identifier','user_info','comment','created_at', 'updated_at')


class AlbumSerializer(serializers.ModelSerializer):
    user_info = serializers.SerializerMethodField('user_data')
    latest_pictures = serializers.SerializerMethodField('pictures_data')
    meta_location = PointField()

    def user_data(self, obj):
        if obj.user:
            serializer = UserMinSerializer(
                obj.user, many=False, read_only=True,
                context={"request": self.context.get("request")})
            return serializer.data
        else:
            return None

    def pictures_data(self, obj):
        pictures = Picture.objects.filter(album=obj.pk,reported=False,hide=False).order_by('order')[:3]
        serializer = PictureSerializer(
            pictures, many=True, read_only=True,
            context={"request": self.context.get("request")})

        return serializer.data


    class Meta:
        model = Album
        read_only_fields = ('user',)
        write_only_fields = ('private',)
        fields = ('id','identifier', 'user_info', 'title', 'meta_location', 
            'address_es', 'address_it', 'address_fr', 'address_en',
            'latest_pictures','created_at', 'updated_at', 'date_range','private')


class CreateAlbumSerializer(serializers.ModelSerializer):
    """
    The only difference of this Serializer, is this one uses "my_item" to create an element.
    """
    user_info = serializers.SerializerMethodField('user_data')
    latest_pictures = serializers.SerializerMethodField('pictures_data')
    meta_location = PointField()

    def user_data(self, obj):
        if obj.user:
            serializer = UserMinSerializer(
                obj.user, many=False, read_only=True,
                context={"request": self.context.get("request")})
            return serializer.data
        else:
            return None

    def pictures_data(self, obj):
        pictures = Picture.objects.filter(album=obj.pk,reported=False,hide=False)[:3]
        serializer = PictureSerializer(
            pictures, many=True, read_only=True,
            context={"request": self.context.get("request")})

        return serializer.data


    class Meta:
        model = Album
        read_only_fields = ('my_item',)
        write_only_fields = ('user',)
        fields = ('id','identifier', 'user','user_info', 'title', 'meta_location', 
            'address_es', 'address_it', 'address_fr', 'address_en',
            'latest_pictures','created_at', 'updated_at','my_item','private')


class HashtagSerializer(serializers.ModelSerializer):
   
    class Meta:
        model = Hashtag
        fields = ('id','name', 'created_at', 'updated_at')        



class HashtagSerializer(serializers.ModelSerializer):
   
    class Meta:
        model = Hashtag
        fields = ('id','name', 'created_at', 'updated_at')        


class UsertagSerializer(serializers.ModelSerializer):
    user_info = serializers.SerializerMethodField('user_data')

    def user_data(self, obj):
        if obj.user:
            serializer = UserMinSerializer(
                obj.user, many=False, read_only=True,
                context={"request": self.context.get("request")})
            return serializer.data
        else:
            return None
       
    class Meta:
        model = Usertag
        fields = ('id', 'identifier', 'user', 'user_info','picture','top','left','non_user','created_at', 'updated_at','hide')



class MediaItemSerializer(serializers.ModelSerializer):
    item_info = serializers.SerializerMethodField('item_data')
    item_type = serializers.SerializerMethodField('item_type_data')

    def item_data(self, obj):
        if obj.picture:
            serializer = PictureSerializer(
                obj.picture, many=False, read_only=True,
                context={"request": self.context.get("request")})
            return serializer.data
        elif obj.album:
            serializer = AlbumSerializer(
                obj.album, many=False, read_only=True,
                context={"request": self.context.get("request")})
            return serializer.data
        else:
            return None

    def item_type_data(self, obj):
        if obj.picture:
            return 'picture'
        elif obj.album:
            return 'album'
        else:
            return None
    class Meta:
        model = UserComment
        fields = ('pk','item_info','item_type','created_at', 'updated_at')


class ReportItemSerializer(serializers.ModelSerializer):
       
    class Meta:
        model = ReportItem
        read_only_fields = ('checked',)

        fields = ('id', 'identifier', 'user', 'picture','checked','comment','created_at', 'updated_at')
