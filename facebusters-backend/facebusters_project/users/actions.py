import csv
from django.http import HttpResponse


def export_as_csv_action(description="Export selected objects as CSV file", fields=None, exclude=None, header=True):
    """
    This function returns an export csv action
    'fields' and 'exclude' work like in django ModelForm
    'header' is whether or not to output the column names as the first row
    """
    def export_as_csv(modeladmin, request, queryset):
        """
        Generic csv export admin action.
        based on http://djangosnippets.org/snippets/1697/
        """
        opts = modeladmin.model._meta
        headers = []
        fields_verbose = {}
        field_names = set([field.name for field in opts.fields])
        for field in opts.fields:
            fields_verbose[field.name] = field.verbose_name
        if fields:
            fieldset = set(fields)
            field_names = field_names & fieldset
        elif exclude:
            excludeset = set(exclude)
            field_names = field_names - excludeset

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=%s.csv' % opts.verbose_name_plural

        writer = csv.DictWriter(response, fields)
        for field in fields:
            for k, v in fields_verbose.items():
                if k == field:
                    headers.append(v)
        writer.writerow(
            dict(zip(fields, headers)))
        for obj in queryset:
            writer.writerow(
                dict(zip(fields, [getattr(obj, field) for field in fields])))
        return response
    export_as_csv.short_description = description
    return export_as_csv
