from django.apps import AppConfig


class PicturesConfig(AppConfig):
    name = 'facebusters_project.pictures'
