# encoding:utf-8
import uuid
from django.utils import timezone
import os
import string
import random
import json
import uuid
import base64
from rest_framework import serializers
import geocoder
from facebusters_project.notifications.utils import send_notification_followers, send_notification_tag
from django.conf import settings
import urllib.request

#GIS Imports
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import Distance  

from django.db.models import Count

def get_file_name(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join(instance.directory_string_var, filename)


def strip_seconds(dt):
    return dt.replace(second=0, microsecond=0)


def code_generator(size=8, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


class MultipartM2MField(serializers.Field):
    def to_representation(self, obj):
        return obj.values_list('id', flat=True).order_by('id')

    def to_internal_value(self, data):
        return data.split(',') if data else None


def get_full_address(lat=None,lon=None):

    go = geocoder.google([lat,lon], method='reverse', lang='en')
    # g = geocoder.osm([lat,lon], method='reverse', lang='en')

    formatted_address = go.json['raw']['formatted_address']

    return formatted_address


def get_address_google(address):
    response = {}
    response['results'] = []

    #Make Google API call
    # Data to send
    api_key = settings.GOOGLE_API_KEY
    address_google = urllib.parse.quote_plus(address)

    webURL = urllib.request.urlopen(
        "https://maps.googleapis.com/maps/api/place/autocomplete/json?input="+address_google+"&inputtype=textquery&key="+api_key
    )

    webURLdata = webURL.read()
    encoding = webURL.info().get_content_charset('utf-8')
    respJSON = json.loads(webURLdata.decode(encoding))

    for item in respJSON['predictions']:
        new_item = {}
        new_item['formatted'] = item['description']
        new_item['place_id'] = item['place_id']
        response['results'].append(new_item)

    return response


def get_address_place_id(place_id):
    response = {}
    response['results'] = []

    #Make Google API call
    # Data to send
    api_key = settings.GOOGLE_API_KEY
    #address_google = urllib.parse.quote_plus(address)

    webURL = urllib.request.urlopen(
        "https://maps.googleapis.com/maps/api/geocode/json?place_id="+place_id+"&key="+api_key
    )

    webURLdata = webURL.read()
    encoding = webURL.info().get_content_charset('utf-8')
    respJSON = json.loads(webURLdata.decode(encoding))

    for item in respJSON['results']:
        new_item = {}
        new_item['formatted'] = item['formatted_address']
        new_item['place_id'] = item['place_id']
        new_item['geometry'] = {}
        new_item['geometry']['lat'] = item['geometry']['location']['lat']
        new_item['geometry']['lon'] = item['geometry']['location']['lng']
        response['results'].append(new_item)

    return response


def create_related_object(instance=None,object_type=None):
    # We import the required models here, to avoid cyclic imports
    from facebusters_project.pictures.models import MediaItem, Album, Picture    

    try:
        if object_type == 'album':
            instance = MediaItem.objects.get(album__pk=instance.pk)
        if object_type == 'picture':
            instance = MediaItem.objects.get(picture__pk=instance.pk)
        return True
    except MediaItem.DoesNotExist:
        new_item = MediaItem()
        if object_type == 'album':
            print(type(instance))
            new_item.album = instance
        if object_type == 'picture':
            new_item.picture = instance
        new_item.save()

        if object_type == 'picture':
            send_notification = False
            if instance.album is not None and instance.album.public == True:
                send_notification = True

            if instance.album is None and instance.public == True:
                send_notification = True

            # We only send notifications when a picture is public. 
            if send_notification == True:
                # Send notification to all friends, when user upload a picture. 
                send_notification_followers(instance.user, 'NEW-PICS', new_item)

        return False

def send_notification_location(picture, radius):
    '''
    We get a geo point and send a notification to all users on a radius. 
    '''
    # We import the required models here, to avoid cyclic imports
    from facebusters_project.pictures.models import Picture
    from facebusters_project.users.models import User

    key = "NEW_PICTURES_PLACE"
    items_in_radius = []
    lon = float(picture.meta_location.x)
    lat = float(picture.meta_location.y)
    point = Point(lon, lat)
    
    if picture.public == True:
        queryset = Picture.objects.filter(
            meta_location__distance_lt=(point, Distance(km=radius)),hide=False
            ).exclude(user=picture.user).values('user__id').annotate(Count('user__id'))
        
        print('===================')
        print(queryset)
        print(queryset.query)
        print('===================')

        for user in queryset:
            try:
                current_user = User.objects.get(pk=user['user__id'])
                send_notification_tag(current_user, picture.identifier, key)    
            except User.DoesNotExist: 
                print('User not exists: '+str(user['user__id']))
    return True
