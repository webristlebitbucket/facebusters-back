import environ
from django.apps import apps
from django.utils import timezone
from django.conf import settings

# Notification Models
from facebusters_project.notifications.models import NotificationItem, NotificationLibrary

# Push notifications SDK
import onesignal as onesignal_sdk


def send_push_notification(content,headings,url,player_ids):
    '''Send a push notification to one or multiple users
       content and headings must be dictionaries {"en": "Message",}
       url is a string
       players_id must be an array
    '''
    app_id = settings.ONE_SIGNAL_APP_ID
    app_api_key = settings.ONE_SIGNAL_API_KEY
    print(app_id)
    print(app_api_key)

    onesignal_client = onesignal_sdk.Client(app={"app_auth_key": app_api_key, "app_id": app_id})

    new_notification = onesignal_sdk.Notification(contents=content)
    new_notification.set_parameter("include_player_ids",player_ids)
    new_notification.set_parameter("headings", headings)
    #new_notification.set_parameter("url", url)

    onesignal_response = onesignal_client.send_notification(new_notification)

    print(onesignal_response.status_code)
    print(onesignal_response.json())

    return onesignal_response


def send_notification_followers(user_id, notificationlibrary_key, media_item):  

    UserFollow = apps.get_model(
        app_label='users', model_name='UserFollow')
    NotificationLibrary = apps.get_model(
        app_label='notifications', model_name='NotificationLibrary')
    NotificationItem = apps.get_model(
        app_label='notifications', model_name='NotificationItem')

    try:
        notification_item = NotificationLibrary.objects.get(key=notificationlibrary_key)
    except NotificationLibrary.DoesNotExist:
        print('Notification with KEY '+str(notificationlibrary_key)+' not exists')
        return False

    receivers = UserFollow.objects.filter(follow_to=user_id,active=True)

    for item in receivers:
        print('item debug')
        print(item.pk)
        print(item.user)
        print('===========')
        # Send notification to every follower
        new_notification = NotificationItem()
        new_notification.user = item.user
        new_notification.notification_library = notification_item
        try:
            new_notification.related_item_key = media_item.main_image
        except Exception as error:
            print(error)
        new_notification.status = 'received'
        new_notification.save()
        print('Notification for user '+item.user.email)

    return True


def send_notification_tag(user_id, picture, key):
    # Notify to user when someone tag him in a picture
    NotificationLibrary = apps.get_model(
        app_label='notifications', model_name='NotificationLibrary')
    NotificationItem = apps.get_model(
        app_label='notifications', model_name='NotificationItem')

    try:
        notification_item = NotificationLibrary.objects.get(key=key)
    except NotificationLibrary.DoesNotExist:
        print('Notification with KEY '+str(notificationlibrary_key)+' not exists')
        return False

    new_notification = NotificationItem()
    new_notification.user = user_id
    new_notification.notification_library = notification_item
    new_notification.related_item_key = picture
    new_notification.status = 'received'
    new_notification.save()

    return True  


def send_single_notification_follower(user_id, key):
    # Generic notification to send a single user
    NotificationLibrary = apps.get_model(
        app_label='notifications', model_name='NotificationLibrary')
    NotificationItem = apps.get_model(
        app_label='notifications', model_name='NotificationItem')

    try:
        notification_item = NotificationLibrary.objects.get(key=key)
    except NotificationLibrary.DoesNotExist:
        print('Notification with KEY '+str(notificationlibrary_key)+' not exists')
        return False

    new_notification = NotificationItem()
    new_notification.user = user_id
    new_notification.notification_library = notification_item
    # new_notification.related_item_key = user_follow # Pending to set this working on the app side. We need to add the user here.
    new_notification.status = 'received'
    new_notification.save()

    return True  
