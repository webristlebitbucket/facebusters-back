from background_task import background  # Importar Tarea
from facebusters_project.utils.code_responses import get_response
from django.apps import apps
from django.utils import timezone
from django.core.files.uploadedfile import SimpleUploadedFile
from facebusters_project.utils.utils import send_notification_location

#GIS Imports
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import Distance  

# For picture generation
import subprocess
from django.core.files import File


@background(queue='pictures')  
def apply_filters(picture_pk):  # funcion a ejecutar
    ''' Generate an snapshot of a picture to apply filters'''
    Picture = apps.get_model(
        app_label='pictures', model_name='Picture')

    try:
        picture = Picture.objects.get(identifier=picture_pk)
    except Picture.DoesNotExist:
        print(' PICTURE NOT EXISTS: '+str(picture_pk))
        return True

    if picture:
        '''
        Temporally, we jusst call a bash script from shell
        '''
        # TODO add this to an environment var. 
        
        result = subprocess.run(['/home/facebusters/picture_generator/launcher.sh', picture.identifier], stdout=subprocess.PIPE)
        picture_binary = open('/tmp/'+picture.identifier+'.png', 'rb')
        picture_file = File(picture_binary)

        # Update picture filter image:
        picture.image_with_filter = picture_file
        picture.save()
        
        print('Saved pic: '+str(picture.identifier))
    return True


@background(queue='notifications')  
def location_notification(picture_pk):  
    Picture = apps.get_model(
        app_label='pictures', model_name='Picture')

    try:
        picture = Picture.objects.get(identifier=picture_pk)
    except Picture.DoesNotExist:
        print(' PICTURE NOT EXISTS: '+str(picture_pk))
        return True
    radius = 1 # Define here radius tu use, To improve this we can add this to an app setting.
    send_notification_location(picture, radius)
    #   "NOTIFICATIONS_SPECIAL": "NEW PHOTO CLOSE TO YOUR PLACES",
    return True