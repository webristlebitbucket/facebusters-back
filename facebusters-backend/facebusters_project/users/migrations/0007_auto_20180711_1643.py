# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-07-11 14:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0006_userfollow'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='num_albums',
            field=models.IntegerField(null=True, verbose_name='Albums'),
        ),
        migrations.AddField(
            model_name='user',
            name='num_connections',
            field=models.IntegerField(null=True, verbose_name='Connections'),
        ),
        migrations.AddField(
            model_name='user',
            name='num_followers',
            field=models.IntegerField(null=True, verbose_name='Followers'),
        ),
        migrations.AddField(
            model_name='user',
            name='num_following',
            field=models.IntegerField(null=True, verbose_name='Following'),
        ),
        migrations.AddField(
            model_name='user',
            name='num_pictures',
            field=models.IntegerField(null=True, verbose_name='Pictures'),
        ),
        migrations.AddField(
            model_name='user',
            name='num_tagged_in',
            field=models.IntegerField(null=True, verbose_name='Tagged in'),
        ),
    ]
