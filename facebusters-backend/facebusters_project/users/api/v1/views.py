# Rest framework imports
from rest_framework import viewsets, pagination, mixins
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

# Django Core imports
from django.core.exceptions import PermissionDenied
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.views.generic import DetailView, ListView, RedirectView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q

# Models imports
from facebusters_project.users.models import User, UserFollow

# Serializers imports
from .serializers import UserSerializer, UserFollowSerializer, UserPublicSerializer, UserSearchSerializer

# Utils imports
from facebusters_project.utils.code_responses import get_response
from facebusters_project.utils.utils import code_generator
from facebusters_project.utils.email import sendEmail

# Permissions
from rest_framework.permissions import BasePermission, IsAuthenticated

# Throttling
from rest_framework.throttling import UserRateThrottle

# System imports
import uuid
import re
import random

from io import BytesIO
from urllib.request import urlopen
from django.core.files import File

class UserViewSet(viewsets.ModelViewSet):
    http_method_names = ['get', 'patch']
    # http_method_names = ['retrieve', 'get']
    model_class = User
    queryset = User.objects.all()
    serializer_class = UserSerializer
    paginate_by = 20
    lookup_fields = ('identifier')

    def get_serializer_context(self):
        return {'request': self.request}

    def get_permissions(self):
        self.permission_classes = [IsAuthenticated, ]
        return super(UserViewSet, self).get_permissions()

    def list(self, request, format=None):
        try:
            user = self.model_class.objects.get(pk=self.request.user.pk)
        except self.model_class.DoesNotExist:
            user = self.model_class.objects.none()
        serializer = self.get_serializer(user)
        return Response(serializer.data)
    
    def partial_update(self, request, *args, **kwargs):
        instance = request.user
        if instance:
            if 'old_password' in request.data and 'new_password' in request.data: # Change password
                password = request.data['old_password']
                new_password = request.data['new_password']
                if not instance.check_password(password):
                    return Response({'error': '403 Invalid password'}, status=403)
                if len(new_password) < 5:
                    return Response({'error': '403 Invalid new password, min 5 characters'}, status=403)
                instance.set_password(new_password)
                instance.save()
            serializer = self.serializer_class(instance, data=request.data, partial=True, context={'user': self.request.user, "request": self.request})
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data)
        return Response({'error': '401 Unauthorized'}, status=401)

    def retrieve(self, request, pk=None):
        instance = self.model_class.objects.filter(
            **{self.lookup_fields: pk}).filter(username=request.user)
        if len(instance) == 1:
            serializer = self.get_serializer(instance[0])
            return Response(serializer.data)
        else:
            response = get_response('USER_NOT_EXISTS')
            return Response(data=response['data'], status=response['status'])

class UserFollowViewSet(viewsets.ModelViewSet, mixins.ListModelMixin):
    http_method_names = ['get','post']
    model_class = UserFollow
    queryset = UserFollow.objects.all()
    serializer_class = UserFollowSerializer
    paginate_by = 20

    def get_permissions(self):
        self.permission_classes = [IsAuthenticated, ]
        return super(UserFollowViewSet, self).get_permissions()

    def list(self, request, format=None):
        query = request.query_params
        if 'following' in query.keys() or 'followers' in query.keys():
            if 'following' in query.keys():
                users = UserFollow.objects.filter(user=request.user, active=True)
            elif 'followers' in query.keys():
                users = UserFollow.objects.filter(follow_to=request.user, active=True)
            else:
                users = UserFollow.objects.none()
        else:
            users = UserFollow.objects.none()

        # Exclude deleted users
        users = users.exclude(Q(user__delete_account=True) | Q(follow_to__delete_account=True))

        serializer = UserFollowSerializer(
            data=users, partial=True, many=True, context={'user': request.user, "request": self.request})
        serializer.is_valid(raise_exception=False)
        return Response(serializer.data)

    def create(self, request, format=None):
        '''
        On this view you can:
            - Add follower
            - Remove follower

        We receive:
        {   
            user:PK
            status: follow/unfollow
        }
        '''

        if not request.user:
            response = get_response('USER_NOT_EXISTS')
            return Response(data=response['data'], status=response['status'])
        user = User.objects.get(pk=self.request.user.pk)

        if user.delete_account == True:
            response = get_response('DELETED_USER')
            return Response(data=response['data'], status=response['status'])

        info = request.data


        if 'user' in info and 'status' in info:

            # Check if user already exists on our BBDD
            try:
                follow_user = User.objects.get(identifier=info['user'])
            except User.DoesNotExist:
                follow_user = None
                response = get_response('USER_TO_FOLLOW_NOT_EXISTS')
                return Response(data=response['data'], status=response['status'])

            # Check if follow relation exists and is active on our BBDD
            if info['status'] == 'unfollow':
                try:
                    current_follow = UserFollow.objects.get(user=user,follow_to=follow_user, active=True)
                except UserFollow.DoesNotExist:
                    current_follow = None
                    response = get_response('YOU_ARENT_FOLLOWING_THIS_USER')
                    return Response(data=response['data'], status=response['status'])

                # Disable this following relationship
                if current_follow is not None:
                    current_follow.active = False
                    current_follow.save()

                response = get_response('UNFOLLOW_DONE')
                return Response(data=response['data'], status=response['status'])        


            # Check if follow relation exists and is active on our BBDD
            if info['status'] == 'follow':
                try:
                    current_follow = UserFollow.objects.get(user=user,follow_to=follow_user, active=True)
                except UserFollow.DoesNotExist:
                    current_follow = None
                    #response = get_response('USER_NOT_EXISTS')
                    #return Response(data=response['data'], status=response['status'])

                # Disable this following relationship
                if current_follow is not None:
                    response = get_response('USER_ALREADY_FOLLOWED')
                    return Response(data=response['data'], status=response['status'])                   

                else:
                    # Create this following relation
                    new_follow = UserFollow()
                    new_follow.user = user
                    new_follow.follow_to = follow_user
                    new_follow.active = True
                    new_follow.save()

                    response = get_response('FOLLOW_DONE')
                    return Response(data=response['data'], status=response['status'])        
        else:
            response = get_response('REQUIRED_FIELDS')
            return Response(data=response['data'], status=response['status'])     


class ResetPassword(APIView):
    permission_classes = (AllowAny,)
    throttle_classes = (UserRateThrottle,)

    def post(self, request, format=None):
        received = request.data
        if 'email' in received.keys():
            try:
                # TODO: hay que fixear que no hay 2 usuarios con el mismo mail
                user = User.objects.get(email=received['email'])

                if user.delete_account == True:
                    response = get_response('DELETED_USER')
                    return Response(data=response['data'], status=response['status'])

                user.password_token = str(random.randint(10000,99999))
                user.save()
                # Send Mail confirmation
                valuesToReplaceOnEmailTemplate = {
                    'username': user.username, 
                    'password': user.password_token
                }
                sendEmail(
                    received['email'],'RESET_PASSWORD', valuesToReplaceOnEmailTemplate, user.language
                )
                print('mail ok')
                response = get_response('PASSWORD_UPDATE_OK')

            except User.DoesNotExist:
                print('usuario no existe')
                response = get_response(400115)
        else:
            response = get_response('FIELDS_MISSING')
            response['data']['fields_missing'] = ["email"]

        return Response(data=response['data'],
                        status=response['status'])


class ResetPasswordFromMailCode(APIView):
    permission_classes = (AllowAny,)
    throttle_classes = (UserRateThrottle,)

    def post(self, request, format=None):
        received = request.data
        if 'email' in received.keys() and 'code' in received.keys() and 'new_password' in received.keys():
            try:
                user = User.objects.get(email=received['email'], password_token=received['code'])

                if user.delete_account == True:
                    response = get_response('DELETED_USER')
                    return Response(data=response['data'], status=response['status'])

                user.set_password(str(received['new_password']))
                user.save()
                # Send Mail confirmation
                valuesToReplaceOnEmailTemplate = {
                    'username': user.username,
                }
                sendEmail(
                    received['email'],'PASSWORD_IS_CHANGED', valuesToReplaceOnEmailTemplate, user.language
                )
                response = get_response('PASSWORD_UPDATE_OK')

            except User.DoesNotExist:
                response = get_response(400115)
        else:
            response = get_response('FIELDS_MISSING')
            response['data']['fields_missing'] = ["email", "code", "new_password"]

        return Response(data=response['data'],
                        status=response['status'])


class UserPublicViewSet(viewsets.ModelViewSet):
    http_method_names = ['retrieve','get']
    model_class = User
    queryset = User.objects.all().exclude(delete_account=True)
    serializer_class = UserPublicSerializer
    paginate_by = 20
    lookup_fields = ('identifier')

    def get_serializer_context(self):
        return {'request': self.request}
        
    def get_permissions(self):
        self.permission_classes = [IsAuthenticated, ]
        return super(UserPublicViewSet, self).get_permissions()

    def retrieve(self, request, pk=None):
        instance = self.model_class.objects.filter(
            **{self.lookup_fields: pk})
        if len(instance) == 1:
            serializer = self.get_serializer(instance[0])
            return Response(serializer.data)
        else:
            response = get_response('USER_NOT_EXISTS')
            return Response(data=response['data'], status=response['status'])


class UserSearchViewSet(viewsets.ModelViewSet):
    http_method_names = ['get',]
    model_class = User
    queryset = User.objects.all()
    serializer_class = UserSearchSerializer
    paginate_by = 20
    lookup_fields = ('identifier')

    def get_permissions(self):
        self.permission_classes = [IsAuthenticated, ]
        return super(UserSearchViewSet, self).get_permissions()

    def list(self, request, format=None):
        query = request.query_params
        if 'text' in query.keys() and len(query['text']) >= 3:
            users = User.objects.filter(
                (
                    Q(username__icontains=query['text']) |
                    Q(first_name__icontains=query['text'])|
                    Q(last_name__icontains=query['text'])
                ) & Q(is_staff=False)
            ).exclude(delete_account=True)[:20]
        else:
            users = User.objects.none()

        serializer = UserSearchSerializer(
            data=users, partial=True, many=True, context={'user': request.user, "request": self.request})
        serializer.is_valid(raise_exception=False)
        return Response(serializer.data)



class Register(APIView):
    permission_classes = (AllowAny,)
    throttle_classes = (UserRateThrottle,)

    def post(self, request, format=None):
        received = request.data
        requiredFields = ['email', 'username', 'avatar', 'name', 'last_name', 'password']

        # Check all fields are received
        response = get_response('FIELDS_MISSING')
        response['data']['fields_missing'] = []
        for requiredField in requiredFields:  
            if requiredField not in received.keys():
                response['data']['fields_missing'].append([requiredField])
        if len(response['data']['fields_missing'])>0: 
            return Response(data=response['data'], status=response['status'])
        
        # Validations
        if not re.match(r"[^@]+@[^@]+\.[^@]+", str(received['email'])):
            response = get_response('EMAIL_WRONG')
            return Response(data=response['data'], status=response['status'])
        if len(received['username'])<5:
            response = get_response('USERNAME_WRONG')
        if User.objects.filter(username=received['username']).count() >0:
            response = get_response('USERNAME_EXISTS')
            return Response(data=response['data'], status=response['status'])
        if User.objects.filter(username=received['email']).count() >0:
            response = get_response('EMAIL_EXISTS')
            return Response(data=response['data'], status=response['status'])
        if len(received['name'])<4:
            response = get_response('NAME_WRONG')
            return Response(data=response['data'], status=response['status'])
        if len(received['last_name'])<4:
            response = get_response('LAST_NAME_WRONG')
            return Response(data=response['data'], status=response['status'])
        if len(received['password'])<5:
            response = get_response('PASSWORD_WRONG')
            return Response(data=response['data'], status=response['status'])
        
        language_device = None
        try:
            print('- LANGUAGE ==> ')
            print(received['language'])
            if received['language']:
                if received['language'].upper().startswith( 'EN' ):
                    language_device = 'EN'
                if received['language'].upper().startswith( 'ES' ):
                    language_device = 'ES'
                if received['language'].upper().startswith( 'IT' ):
                    language_device = 'IT'
                if received['language'].upper().startswith( 'RU' ):
                    language_device = 'EU'
        except Exception as error:
            print(error)
            language_device = 'EN'

        # User Creation
        new_user = User() 
        new_user.email = received['email']
        new_user.username = received['username']
        new_user.name = received['name']
        new_user.first_name = received['first_name']
        new_user.last_name = received['last_name']
        new_user.set_password(received['password'])
        new_user.verification_code = str(random.randint(10000, 99999))
        new_user.language = language_device
        new_user.save()

        # Update only user avatar with serializer and bs64 picture.
        data = {}
        data['avatar'] = received['avatar']
        serializer = UserSerializer(new_user, data=data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        # Send Mail confirmation
        valuesToReplaceOnEmailTemplate = {
            'username': new_user.username, 
            'verification_code': new_user.verification_code
        }
        sendEmail(
            new_user.email,'VERIFICATION_ACCOUNT', valuesToReplaceOnEmailTemplate, new_user.language
        )

        response = get_response('USER_CREATED')
        return Response(data=response['data'],
                        status=response['status'])

class ConfirmationAccount(APIView):
    permission_classes = (AllowAny,)
    throttle_classes = (UserRateThrottle,)

    def post(self, request, format=None):
        ''' 
            Post
            args: verification_code, email
        '''
        received = request.data
        user = None
        try:
            user = User.objects.get(
                email= received['email'],
                verification_code=received['verification_code']
            )
            if user.delete_account == True:
                response = get_response('DELETED_USER')
                return Response(data=response['data'], status=response['status'])

        except Exception as error:
            response = get_response('USER_NOT_EXISTS')
            return Response(data=response['data'], status=response['status'])
        # Validations
        if user.account_verified:
            response = get_response('ACCOUNT_ALREADY_VERIFIED')
            return Response(data=response['data'], status=response['status'])
        if user.verification_code != received['verification_code']:
            response = get_response('VERIFICATION_CODE_WRONG')
            return Response(data=response['data'], status=response['status'])

        # Activate account
        user.account_verified = True
        user.save()

        # Send Mail account is verified ok
        valuesToReplaceOnEmailTemplate = {'username': user.username}
        sendEmail(
            user.email,'ACCOUNT_VERIFIED', valuesToReplaceOnEmailTemplate, user.language
        )
        response = get_response('VERIFY_OK')
        return Response(data=response['data'], status=response['status'])

class ResendEmailConfirmationAccount(APIView):
    ''' 
        Post
        args: email
    '''
    permission_classes = (AllowAny,)
    throttle_classes = (UserRateThrottle,)

    def post(self, request, format=None):
        received = request.data
        user = None
        try:
            user = User.objects.get(email=received['email'])
            if user.delete_account == True:
                response = get_response('DELETED_USER')
                return Response(data=response['data'], status=response['status'])

        except Exception as error:
            response = get_response('USER_NOT_EXISTS')
            return Response(data=response['data'], status=response['status'])
        # Validations
        if user.account_verified:
            response = get_response('ACCOUNT_ALREADY_VERIFIED')
            return Response(data=response['data'], status=response['status'])

        # Send Mail confirmation
        valuesToReplaceOnEmailTemplate = {
            'username': user.username, 
            'verification_code': user.verification_code
        }
        sendEmail(
            new_user.email,'VERIFICATION_ACCOUNT', valuesToReplaceOnEmailTemplate, user.language
        )
        response = get_response('VERIFY_OK')
        return Response(data=response['data'], status=response['status'])



class LoginWithSocial(APIView):
    ''' 
        provider : 'facebook | google'
        data: json
    '''
    permission_classes = (AllowAny,)
    
    # TO FIX, why are doing all fb logic on frontennd without check again on backend. 
    def post(self, request, format=None):
        received = request.data
        requiredFields = ['provider', 'data']
        status = None

        # Check required fields
        response = get_response('FIELDS_MISSING')
        response['data']['fields_missing'] = []

        for requiredField in requiredFields:  
            if requiredField not in received.keys():
                response['data']['fields_missing'].append([requiredField])

        if len(response['data']['fields_missing'])>0: 
            return Response(data=response['data'], status=response['status'])

        # Setup data
        provider = received['provider'].upper()
        data = received['data']
        email = None
        name = None
        lastname = None
        avatar = None
        password = None

        # Login or Register checking
        if(provider == 'FACEBOOK'):
            email = data['email'].lower()
            name = data['name']
            lastname = ''
            avatar = data['picture']['data']['url']
            password = str(random.randint(1000, 9999)) + '-password'
        elif(provider == 'GOOGLE'):
            email = data['email'].lower()
            name = data['givenName']
            lastname = data['familyName']
            avatar = data['imageUrl']
            password = str(random.randint(1000, 9999)) + '-password'

        try:
            user = User.objects.get(email=email)
            status = 'login_user'
        except User.DoesNotExist:  # User not exists ==> register user
            user = User() 
            user.social_provider = provider
            user.social_provider_data = data
            user.email = email
            user.username = name + '_' + str(random.randint(1000, 9999)).lower()
            user.name = name
            user.first_name = lastname

            # Save user profile pic
            r = urlopen(avatar)
            io = BytesIO(r.read())
            user.avatar.save(user.username+'.jpg', File(io))
            user.set_password(password)
            user.verification_code = str(random.randint(10000, 99999))
            user.save()
            status = 'new_user'

        token = Token.objects.get_or_create(user=user)

        return Response({
            'token': str(token[0]),
            'user_id': user.pk,
            'status': status
        })
                

class CheckEmailExists(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        received = request.data
        if 'email' not in received.keys():
            print('no hay email')
            response = get_response('EMAIL_EXISTS')
            return Response(data=response['data'], status=response['status'])
        else:
            print(received['email'])
            
            try:
                user = User.objects.filter(email=received['email'])[0]
                print(user.email)
                print('email ya exxiste')

                response = get_response('EMAIL_EXISTS')
                return Response(data=response['data'], status=response['status'])
            except Exception as error:
                pass
            print('email disponible')

            response = get_response('EMAIL_NOT_EXISTS')
            return Response(data=response['data'], status=response['status'])

class MatchFacebookFriends(APIView):
    '''
        We receive a list of emails and create a follow object
        [
            {   
                email:email_user
            },
            {   
                email:email_user
            },
            ...
        ]
    '''
    
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        received = request.data

        if request.user.delete_account == True:
            response = get_response('DELETED_USER')
            return Response(data=response['data'], status=response['status'])        

        users = [] # List of existing users
        for item in received:
            print(item)
            try: # Check if user already exists
                new_user = User.objects.get(email=item['email'])
                users.append(new_user.pk)
            except User.DoesNotExist:
                pass
        print(users)

        # Get users array and check if they exists on our database.
        current_followings = UserFollow.objects.filter(user=request.user,active=True).values_list('follow_to__pk')  # Get only active follows.
        print(current_followings)
        # Get request user ACTIVES follows_to

        # with a loop check if already follows and if not, follow him
        # Exclude current followings:
        people_to_follow = User.objects.filter(pk__in=users).exclude(pk__in=current_followings).exclude(pk=request.user.pk)
        print(people_to_follow)

        followeds = []
        for user in people_to_follow:
            new_follow = UserFollow()
            new_follow.user = request.user
            new_follow.follow_to = user
            new_follow.fb_follow = True
            new_follow.save()

            followeds.append({"email":new_follow.follow_to.email})

        return Response({
            'follows': followeds
        })