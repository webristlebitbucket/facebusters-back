# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-12-04 14:54
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pictures', '0027_auto_20181130_2036'),
    ]

    operations = [
        migrations.AddField(
            model_name='album',
            name='order',
            field=models.IntegerField(default=0, null=True, verbose_name='Order'),
        ),
    ]
