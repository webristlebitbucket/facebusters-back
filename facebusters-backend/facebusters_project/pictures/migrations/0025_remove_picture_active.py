# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-11-29 16:06
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pictures', '0024_picture_active'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='picture',
            name='active',
        ),
    ]
