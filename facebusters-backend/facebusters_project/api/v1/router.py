from rest_framework import routers
from rest_framework.authtoken import views
from facebusters_project.utils.hybridrouter import HybridRouter
from django.conf.urls import include, url
from django.conf import settings

from facebusters_project.users.api.v1.views import (
    UserViewSet, 
    UserFollowViewSet, 
    ResetPassword,
    ResetPasswordFromMailCode,
    Register, 
    ConfirmationAccount,
    ResendEmailConfirmationAccount,
    UserPublicViewSet, 
    UserSearchViewSet,
    LoginWithSocial,
    MatchFacebookFriends,
    CheckEmailExists
)

from facebusters_project.pictures.api.v1.views import (
    PictureViewSet,
    CommentViewSet,
    AlbumViewSet,
    HashtagViewSet, 
    UserTagViewSet,
    TimelineViewSet,
    ReportItemViewSet,
    GetAddress,
    AcceptUserTag
)

from facebusters_project.notifications.api.v1.views import (
    NotificationViewSet,
    NotificationBulkUpdateApiView,
    NotificationCounterApiView,
    UserDeviceApiView
)

# Auth calls
urlpatterns = [
    url(r'^api-token-auth/', views.obtain_auth_token),
]
router = HybridRouter()

# User related views
router.register(r'user', UserViewSet, 'User')
router.register(r'public_profile', UserPublicViewSet, 'PublicProfile')
router.register(r'user_search', UserSearchViewSet, 'UserSearch')
router.register(r'user_follow', UserFollowViewSet, 'Follow')
router.add_api_view("reset_password", url(r'^reset_password/$',
                    ResetPassword.as_view(), name='reset_password'))
router.add_api_view("reset_password_from_mail_code", url(r'^reset_password_from_mail_code/$',
                    ResetPasswordFromMailCode.as_view(), name='reset_password_from_mail_code'))
router.add_api_view("register", url(r'^register/$',
                    Register.as_view(), name='register'))
router.add_api_view("verify_account", url(r'^verify_account/$',
                    ConfirmationAccount.as_view(), name='verify_account'))
router.add_api_view("resend_confirmation_email", url(r'^resend_confirmation_email/$',
                    ResendEmailConfirmationAccount.as_view(), name='resend_confirmation_email'))
router.add_api_view("login_social", url(r'login_social/$',
                    LoginWithSocial.as_view(), name='login_facebook'))
router.add_api_view("accept_usertag", url(r'^accept_usertag/$',
                    AcceptUserTag.as_view(), name='accept_usertag'))
router.add_api_view("check_email_exists", url(r'^check_email_exists/$',
                    CheckEmailExists.as_view(), name='check_email_exists'))
                    
                    
# Pictures related views
router.register(r'picture', PictureViewSet, 'Picture')
router.register(r'comment', CommentViewSet, 'Comment')
router.register(r'album', AlbumViewSet, 'Album')
router.register(r'hashtag', HashtagViewSet, 'Hashtag')
router.register(r'usertag', UserTagViewSet, 'UserTagViewSet')
router.register(r'timeline', TimelineViewSet, 'TimelineViewSet')
router.register(r'report', ReportItemViewSet, 'ReportItemViewSet')

# Location Views
router.add_api_view("get_address", url(r'^get_address/$',
                    GetAddress.as_view(), name='get_address'))

# Notification related views
router.register(r'notification', NotificationViewSet, 'Notification')
router.add_api_view("bulk-notification", url(r'^bulk-notification/$',
                                             NotificationBulkUpdateApiView.as_view(), name='bulkNotification'))
router.add_api_view("notification-count", url(r'^notification-count/$',
                                              NotificationCounterApiView.as_view(), name='countNotification'))
router.add_api_view("subscribe_device", url(r'^subscribe_device/$',
                                              UserDeviceApiView.as_view(), name='subscribe_device'))

# Match Facebook Friends
router.add_api_view("facebook_match", url(r'^facebook_match/$', MatchFacebookFriends.as_view(), name='facebook_match'))

urlpatterns += router.urls
