from django.db.models import Q, Count
from django.db.models.functions import ExtractDay
from django.utils import timezone
from datetime import datetime
from django import template
from facebusters_project.pictures.models import Album, Picture, ReportItem, MediaItem
from facebusters_project.users.models import User
from calendar import monthrange
from easy_thumbnails.files import get_thumbnailer
register = template.Library()


@register.filter(name='timestamp_to_date')
def timestamp_to_date(timestamp):
    try:
        ts = float(timestamp)
    except ValueError:
        return None
    return datetime.datetime.fromtimestamp(ts)


@register.assignment_tag
def get_last_five_pictures():
    last_five_pictures = Picture.objects.order_by('-pk')[0:5]
    return last_five_pictures


@register.assignment_tag
def get_last_five_users():
    last_five_users = User.objects.order_by('-pk')[0:5]
    return last_five_users


def obtain_model_instances_per_day(model, month, year):
    instances_each_month = model.objects.filter(created_at__month=month,created_at__year=year)

    # days_with_instances is [(10,1), (15,2)] if there's 1 instance on day 10 and 2 instances on day 15.

    days_with_instances = instances_each_month.annotate(day=ExtractDay('created_at')).values_list("day").annotate(Count('day'))
    days_instances_array = [x[0] for x in days_with_instances]
    instances_array = [x[1] for x in days_with_instances]

    # Zero instances in any given day as default

    instances_per_day = [0 for x in range(monthrange(int(year), int(month))[1])]
    for ind, day in enumerate(days_instances_array):
        instances_per_day[day] = instances_array[ind]

    return instances_per_day


@register.assignment_tag
def get_pictures_albums_each_month(month, year):
    if month == '':
        month = datetime.now().month
    if year == '':
        year = datetime.now().year

    all_days = [x+1 for x in range(monthrange(int(year), int(month))[1])]

    pictures_per_day = obtain_model_instances_per_day(Picture, month, year)
    albums_per_day = obtain_model_instances_per_day(Album, month, year)

    try:
        pics_ordered_by_date = Picture.objects.order_by('created_at')
        all_years = range(pics_ordered_by_date.first().created_at.year, pics_ordered_by_date.last().created_at.year+1)
    except AttributeError:
        all_years = [datetime.now().year]

    return {
        'all_days': all_days,
        'pictures_per_day': pictures_per_day,
        'albums_per_day': albums_per_day,
        'all_months': range(1,13),
        'all_years': all_years,
        'current_date': {
            'month': datetime.now().month,
            'year': datetime.now().year
        }
    }


@register.assignment_tag
def get_last_five_reportitems_unchecked():
    last_five_reports = ReportItem.objects.filter(checked=False)[0:5]
    return last_five_reports