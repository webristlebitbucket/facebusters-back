from rest_framework import status

responses_nok = {
    'WRONG_DNI': {
        'data': {
            'code': 400101,
            'message': 'Wrong DNI',
            'key': 'WRONG_DNI'
        },
        'status': status.HTTP_400_BAD_REQUEST
    },
    'USER_NOT_EXISTS': {
        'data': {
            'code': 400102,
            'message': 'User not exists',
            'key': 'USER_NOT_EXISTS'
        },
        'status': status.HTTP_400_BAD_REQUEST
    },    
    'USER_ALREADY_FOLLOWED': {
        'data': {
            'code': 400103,
            'message': 'User already followed',
            'key': 'USER_ALREADY_FOLLOWED'
        },
        'status': status.HTTP_400_BAD_REQUEST
    },    
    'REQUIRED_FIELDS': {
        'data': {
            'code': 400104,
            'message': 'You must provide all required fields',
            'key': 'REQUIRED_FIELDS'
        },
        'status': status.HTTP_400_BAD_REQUEST
    },
    'USER_TO_FOLLOW_NOT_EXISTS': {
        'data': {
            'code': 400105,
            'message': 'User to follow not exists',
            'key': 'USER_TO_FOLLOW_NOT_EXISTS'
        },
        'status': status.HTTP_400_BAD_REQUEST
    },
    'YOU_ARENT_FOLLOWING_THIS_USER': {
        'data': {
            'code': 400106,
            'message': 'You arent following this user',
            'key': 'YOU_ARENT_FOLLOWING_THIS_USER'
        },
        'status': status.HTTP_400_BAD_REQUEST
    },
    'PICTURE_NOT_EXISTS': {
        'data': {
            'code': 400107,
            'message': 'This picture not exists',
            'key': 'PICTURE_NOT_EXISTS'
        },
        'status': status.HTTP_400_BAD_REQUEST
    },
    'USERTAG_NOT_EXISTS': {
        'data': {
            'code': 400108,
            'message': 'This tag not exists',
            'key': 'USERTAG_NOT_EXISTS'
        },
        'status': status.HTTP_400_BAD_REQUEST
    },
    'USERTAG_ALREADY_EXISTS': {
        'data': {
            'code': 400109,
            'message': 'This tag already exists',
            'key': 'USERTAG_ALREADY_EXISTS'
        },
        'status': status.HTTP_400_BAD_REQUEST
    },    
    'UNAVAILABLE_STATUS': {
        'data': {
            'code': 400110,
            'message': 'Unavailable status',
            'key': 'UNAVAILABLE_STATUS'
        },
        'status': status.HTTP_400_BAD_REQUEST
    },    
    'USERREPORT_ALREADY_EXISTS': {
        'data': {
            'code': 400111,
            'message': 'Report already exist for this user and picture',
            'key': 'USERREPORT_ALREADY_EXISTS'
        },
        'status': status.HTTP_400_BAD_REQUEST
    },
    'EMAIL_WRONG': {
        'data': {
            'code': 400112,
            'message': 'Email is wrong example@example.example',
            'key': 'EMAIL_WRONG'
        },
        'status': status.HTTP_400_BAD_REQUEST
    },
    'USERNAME_WRONG': {
        'data': {
            'code': 400113,
            'message': 'Username is incorrect try another (min length 5)',
            'key': 'USERNAME_WRONG'
        },
        'status': status.HTTP_400_BAD_REQUEST
    },
    'USERNAME_EXISTS': {
        'data': {
            'code': 400114,
            'message': 'Username is already in use, can try with another',
            'key': 'USERNAME_EXISTS'
        },
        'status': status.HTTP_400_BAD_REQUEST
    },
    'NAME_WRONG': {
        'data': {
            'code': 400115,
            'message': 'Name min length is 4',
            'key': 'NAME_WRONG'
        },
        'status': status.HTTP_400_BAD_REQUEST
    },
    'LAST_NAME_WRONG': {
        'data': {
            'code': 400116,
            'message': 'Last Name min length is 4',
            'key': 'LAST_NAME_WRONG'
        },
        'status': status.HTTP_400_BAD_REQUEST
    },
    'PASSWORD_WRONG': {
        'data': {
            'code': 400117,
            'message': 'Password min length is 5',
            'key': 'PASSWORD_WRONG'
        },
        'status': status.HTTP_400_BAD_REQUEST
    },
    'VERIFICATION_CODE_WRONG': {
        'data': {
            'code': 400118,
            'message': 'Verification Code is wrong',
            'key': 'VERIFICATION_CODE_WRONG'
        },
        'status': status.HTTP_400_BAD_REQUEST
    },
    'ACCOUNT_ALREADY_VERIFIED': {
        'data': {
            'code': 400119,
            'message': 'This account is already verified',
            'key': 'ACCOUNT_ALREADY_VERIFIED'
        },
        'status': status.HTTP_400_BAD_REQUEST
    },
    'EMAIL_EXISTS': {
        'data': {
            'code': 400120,
            'message': 'Email is already in use, can try with another',
            'key': 'EMAIL_EXISTS'
        },
        'status': status.HTTP_400_BAD_REQUEST
    },
    'DELETED_USER': {
        'data': {
            'code': 400121,
            'message': "You can't use this user, has been deleted",
            'key': 'DELETED_USER'
        },
        'status': status.HTTP_400_BAD_REQUEST
    },
    'DEVICE_NOT_EXISTS': {
        'data': {
            'code': 400122,
            'message': "This device not exists",
            'key': 'DEVICE_NOT_EXISTS'
        },
        'status': status.HTTP_400_BAD_REQUEST
    },
    'DEVICE_ALREADY_EXISTS': {
        'data': {
            'code': 400123,
            'message': "This device already exists",
            'key': 'DEVICE_ALREADY_EXISTS'
        },
        'status': status.HTTP_400_BAD_REQUEST
    }
}
