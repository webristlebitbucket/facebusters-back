from django import forms
from django.contrib import admin
from easy_thumbnails.files import get_thumbnailer

# Register your models here.
from .models import Picture, Usertag, Hashtag, UserComment, Album, MediaItem, ReportItem

#Maps Widget
from django.contrib.gis.db import models
from django.contrib.gis import admin as geoadmin

@admin.register(Picture)
class PictureAdmin(geoadmin.OSMGeoAdmin):
    def picture_url(self, obj):
        if obj.image:
            label = u'<center><img src="{0}" class="avatar_list"/></center>'.format(get_thumbnailer(obj.image)['avatar_mini'].url,)
            return label
        else:
            return None

    picture_url.short_description = 'Picture'
    picture_url.allow_tags = True    

    def tagged_in(self, obj):
        label = u'<a href=/admin/pictures/usertag/?picture={0}>Tagged in</a>'.format(obj.pk,)
        return label
    tagged_in.short_description = 'View Followers'
    tagged_in.allow_tags = True


    def picture_comments(self, obj):
        label = u'<a href=/admin/pictures/usercomment/?picture={0}>View comments</a>'.format(obj.pk,)
        return label
    picture_comments.short_description = 'View Comments'
    picture_comments.allow_tags = True

    def reported_by(self, obj):
        num_reports = ReportItem.objects.filter(picture__pk=obj.pk, checked=False).count()
        label = u'<a href=/admin/pictures/reportitem/?picture={0}>Reported ({1})</a>'.format(obj.pk,num_reports)
        return label
    reported_by.short_description = 'Reported by'
    reported_by.allow_tags = True

    readonly_fields = ('picture_url','tagged_in','picture_comments')
    list_display = ('picture_url','user', 'public','meta_date','meta_location',
    'album','tagged_in', 'reported', 'picture_comments','picture_filter',
    'reported_by', 'order', 'created_at','identifier')



@admin.register(Usertag)
class UsertagAdmin(admin.ModelAdmin):
    list_display = ('user', 'non_user', 'picture', 'active')



@admin.register(Hashtag)
class HashtagAdmin(admin.ModelAdmin):

    def tagged_in(self, obj):
        label = u'<a href=/admin/pictures/picture/?hashtags={0}>View pictures</a>'.format(obj.pk,)
        return label
    tagged_in.short_description = 'Pictures with this hashtag'
    tagged_in.allow_tags = True

    readonly_fields = (
        'tagged_in',)

    list_display = ('identifier','name', 'tagged_in','created_at','updated_at')    



@admin.register(UserComment)
class HashtagAdmin(admin.ModelAdmin):
    list_display = ('identifier','user', 'picture','comment','created_at','updated_at')    


@admin.register(Album)
class AlbumAdmin(geoadmin.OSMGeoAdmin):
    def tagged_in(self, obj):
        label = u'<a href=/admin/pictures/picture/?album={0}>View pictures</a>'.format(obj.pk,)
        return label
    tagged_in.short_description = 'Pictures in this album'
    tagged_in.allow_tags = True

    readonly_fields = (
        'tagged_in',)    
    list_display = ('user', 'identifier','title', 'tagged_in', 'private')


@admin.register(MediaItem)
class MediaItemAdmin(admin.ModelAdmin):
    list_display = ('identifier','album', 'picture','created_at','updated_at')    

    
@admin.register(ReportItem)
class ReportItemAdmin(admin.ModelAdmin):
    list_display = ('identifier','picture', 'user','checked','created_at','updated_at')    

