# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-10-16 14:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0011_auto_20180724_0957'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='verification_code',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='Verification Code'),
        ),
    ]
