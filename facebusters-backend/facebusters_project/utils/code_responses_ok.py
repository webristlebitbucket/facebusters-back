from rest_framework import status

responses_ok = {
    'VERIFY_OK': {
        'data': {
            'code': 200101,
            'message': 'Email verified',
            'key': 'VERIFY_OK'
        },
        'status': status.HTTP_200_OK
    },
    'SEND_EMAIL_OK': {
        'data': {
            'code': 200102,
            'message': 'Email Sended',
            'key': 'SEND_EMAIL_OK'
        },
        'status': status.HTTP_200_OK
    },
    'PASSWORD_UPDATE_OK': {
        'data': {
            'code': 200103,
            'message': 'Password Updated',
            'key': 'PASSWORD_UPDATE_OK'
        },
        'status': status.HTTP_200_OK
    },
    'UNFOLLOW_DONE': {
        'data': {
            'code': 200104,
            'message': 'Unfollow Done',
            'key': 'UNFOLLOW_DONE'
        },
        'status': status.HTTP_200_OK
    },
    'FOLLOW_DONE': {
        'data': {
            'code': 200105,
            'message': 'Follow Done',
            'key': 'FOLLOW_DONE'
        },
        'status': status.HTTP_200_OK
    },     
    'COMMENT_CREATED': {
        'data': {
            'code': 200106,
            'message': 'Comment Created',
            'key': 'COMMENT_CREATED'
        },
        'status': status.HTTP_200_OK
    },     
    'USERTAG_REMOVED': {
        'data': {
            'code': 200107,
            'message': 'USertag removed',
            'key': 'USERTAG_REMOVED'
        },
        'status': status.HTTP_200_OK
    },
    'USER_CREATED': {
        'data': {
            'code': 200108,
            'message': 'User created',
            'key': 'USER_CREATED'
        },
        'status': status.HTTP_200_OK
    },
    'USER_LOGGED': {
        'data': {
            'code': 200109,
            'message': 'User logged',
            'key': 'USER_LOGGED'
        },
        'status': status.HTTP_200_OK
    },
    'NOTIFICATION_UPDATED_OK': {
        'data': {
            'code': 200125,
            'message': 'Notification Updated',
            'key': 'NOTIFICATION_UPDATED_OK'
        },
        'status': status.HTTP_200_OK
    },
    'USERTAG_ENABLED': {
        'data': {
            'code': 200126,
            'message': 'Usertag Enabled',
            'key': 'USERTAG_ENABLED'
        },
        'status': status.HTTP_200_OK
    },
    'EMAIL_NOT_EXISTS': {
        'data': {
            'code': 200127,
            'message': 'Email is available to use,',
            'key': 'EMAIL_NOT_EXISTS'
        },
        'status': status.HTTP_200_OK
    },
    'DEVICE_REMOVED': {
        'data': {
            'code': 200128,
            'message': 'Device removed',
            'key': 'DEVICE_REMOVED'
        },
        'status': status.HTTP_200_OK
    },
    'DEVICE_ADDED': {
        'data': {
            'code': 200129,
            'message': 'Device Added',
            'key': 'DEVICE_ADDED'
        },
        'status': status.HTTP_200_OK
    }        
}
