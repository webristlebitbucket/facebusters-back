from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.conf import settings
from rest_framework.authtoken.models import Token
import uuid
from easy_thumbnails.files import get_thumbnailer

from facebusters_project.utils.utils import get_file_name
from facebusters_project.pictures.models import Picture, Album, Usertag, MediaItem

from django.db.models import Q
from django.contrib.postgres.fields import JSONField

# Notifications
from facebusters_project.notifications.utils import send_single_notification_follower

@python_2_unicode_compatible
class User(AbstractUser):
    
    name = models.CharField(_('Name'), blank=True, max_length=255)

    identifier = models.CharField(
        editable=False,
        max_length=300,
        null=True,
        blank=False,
        unique=True
    )

    avatar = models.ImageField(
        verbose_name="Imagen de avatar",
        upload_to=get_file_name,
        null=True,
        blank=True
    )

    verification_code = models.CharField(
        verbose_name="Verification Code",
        max_length=10,
        null=True,
        blank=True
    )
    account_verified = models.BooleanField(default=False)

    # Stats Fields
    num_pictures = models.IntegerField(
        verbose_name="Pictures",
        null=False,
        default=0        
    )
    num_albums = models.IntegerField(
        verbose_name="Albums",
        null=False,
        default=0        
    )
    num_tagged_in = models.IntegerField(
        verbose_name="Tagged in",
        null=False,
        default=0        
    )
    num_followers = models.IntegerField(
        verbose_name="Followers",
        null=False,
        default=0        
    )        
    num_following = models.IntegerField(
        verbose_name="Following",
        null=False,
        default=0
    )
    num_connections = models.IntegerField(
        verbose_name="Connections",
        null=False,
        default=0        
    )

    language_choices = [('EN', 'English'), ('ES', 'Spanish'), ('IT', 'Italian'), ('RU', 'Russian')]

    language = models.CharField(
        verbose_name="Language",
        max_length=3,
        null=False,
        default='EN',
        choices=language_choices
    )

    provider_choices = [('FACEBOOK', 'Facebook'), ('GOOGLE', 'Google')]

    social_provider = models.CharField(
        verbose_name="Provider",
        max_length=10,
        null=True,
        default=None,
        choices=provider_choices
    )
    social_provider_data = JSONField(null=True)

    social_facebook_data = JSONField(null=True)

    social_facebook_id = models.CharField(
        verbose_name="Social Facebook ID",
        max_length=255,
        null=True,
        blank=True
    )

    social_facebook_token = models.CharField(
        verbose_name="Social Facebook Token",
        max_length=255,
        null=True,
        blank=True
    )

    password_token = models.CharField(
        verbose_name="Reset Password Token",
        max_length=255,
        null=True,
        blank=True
    )

    delete_account = models.BooleanField(default=False)
    tag_control = models.BooleanField(default=False)
    
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def avatar_url(self):
        if self.avatar:
            url = get_thumbnailer(self.avatar)['avatar_mini'].url
            return url
        else:
            return None

    @property
    def num_tagged_in(self):
        num_tagged_in = Usertag.objects.filter(user=self.pk,active=True,picture__hide=False).count()
        return num_tagged_in

    @property
    def num_pictures(self):
        num_pictures = Picture.objects.filter(user=self.pk,hide=False).count()
        return num_pictures

    @property
    def num_albums(self):
        num_albums = Album.objects.filter(user=self.pk,hide=False).count()
        return num_albums

    @property
    def num_connections(self):
        num_connections = UserFollow.objects.filter((Q(user=self.pk) | Q(follow_to=self.pk)) & Q(active=True)).count()
        return num_connections

    @property
    def num_following(self):
        num_following = UserFollow.objects.filter(user=self.pk, active=True).count()
        return num_following

    @property
    def num_followers(self):
        num_followers = UserFollow.objects.filter(follow_to=self.pk, active=True).count()
        return num_followers        

    @property
    def list_following(self):
        # Exclude "deleted" users
        followings = UserFollow.objects.filter(user=self.pk,active=True).exclude(follow_to__delete_account=True).values_list('follow_to__pk', flat=True)
        return followings

    # For rename images
    directory_string_var = 'user_avatar'
    
    def __str__(self):
        if(self.username):
            return self.username
        return str(self.pk)

    def get_absolute_url(self):
        return reverse('users:detail', kwargs={'username': self.username})

    def save(self, *args, **kwargs):

        if not self.id or not self.identifier:
            self.identifier = "%s" % (uuid.uuid4(),)

        if not self.username and self.email:
            self.username = self.email
        if not self.email and self.username:
            self.email = self.username
        super(User, self).save(*args, **kwargs)


@python_2_unicode_compatible
class UserFollow(models.Model):
    user = models.ForeignKey('users.User', null=True, blank=True,
            verbose_name='User',related_name='user_follow',
            on_delete=models.SET_NULL)
    follow_to = models.ForeignKey('users.User', null=True, blank=True,
            verbose_name='Follow To',related_name='follow_to',
            on_delete=models.SET_NULL)         
    active = models.BooleanField(default=True)
    fb_follow = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    unique_together = (("user", "follow_to"),)


    def __str__(self):
        return 'User {0} follow {1}'.format(self.user.username,self.follow_to.username)


    def save(self, *args, **kwargs):
        '''
        if not self.pk:
            # Inactive until version 2
            send_single_notification_follower(self.follow_to,'NEW-FOLLOWER')
        '''
        super(UserFollow, self).save(*args, **kwargs)


def save_user(sender, instance, **kwargs):
    # Remove tokens for deleted users
    if instance.delete_account == True and instance.is_active == True:
        #Check if have tokens and delete them
        try:
            token = Token.objects.get(user=instance).delete()
            print('Token')
        except Token.DoesNotExist:
            pass

        # Disable user
        instance.is_active = False

pre_save.connect(save_user, sender=User)