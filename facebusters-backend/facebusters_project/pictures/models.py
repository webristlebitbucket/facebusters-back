from django.core.urlresolvers import reverse
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.conf import settings
import uuid
from django.contrib.gis.db import models as geomodels
from easy_thumbnails.files import get_thumbnailer

from facebusters_project.utils.utils import get_file_name, get_full_address, create_related_object

# Geo Cogind info
import geocoder
from django.db.models.signals import post_save


# Async filters
from facebusters_project.pictures.tasks import apply_filters, location_notification


@python_2_unicode_compatible
class Album(models.Model):
    identifier = models.CharField(
        editable=False,
        max_length=300,
        null=False,
        blank=False,
        unique=True
    )

    title = models.CharField(
        editable=True,
        max_length=300,
        null=False,
        blank=False,
        unique=False
    )
    address_es = models.CharField(_('Address ES'), blank=True, max_length=255)
    address_it = models.CharField(_('Address IT'), blank=True, max_length=255)
    address_fr = models.CharField(_('Address FR'), blank=True, max_length=255)
    address_en = models.CharField(_('Address EN'), blank=True, max_length=255)        

    user = models.ForeignKey('users.User', null=True, blank=True,
            verbose_name='User',related_name='album_user',
            on_delete=models.SET_NULL)

    meta_location = geomodels.PointField(null=True, blank=True, spatial_index=True, geography=True)   

    #FIXME Why in picture the bool name is "public" and here is "private"? We should change both to same name.
    
    private = models.BooleanField(default=False)
    reported = models.BooleanField(default=False)
    hide = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)    

    @property
    def my_item(self):
        """
        The purpose of this property is create a related object before the parent is created. 
        This property is only used on the "create" serializer event.
        """
        return create_related_object(self,object_type='album')

    @property
    def date_range(self):
        """
        This property return a range of dates.
        """
        pictures = Picture.objects.filter(album__pk=self.pk).order_by("meta_date")
        if len(pictures)>=2:
            first_date = pictures.first().meta_date.strftime("%d/%m/%Y")
            last_date = pictures.last().meta_date.strftime("%d/%m/%Y")
            response = first_date+" - "+last_date
        elif len(pictures) == 1:
            response = str(pictures.first().meta_date)
        else:
            response = ""     

        return response

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.id:
            self.identifier = "%s" % (uuid.uuid4(),)

        super(Album, self).save(*args, **kwargs)  


@python_2_unicode_compatible
class Hashtag(models.Model):
    identifier = models.CharField(
        editable=False,
        max_length=300,
        null=False,
        blank=False,
        unique=True
    )
    name = models.CharField(
        editable=True,
        max_length=300,
        null=False,
        blank=False,
        unique=True
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)    

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.id:
            self.identifier = "%s" % (uuid.uuid4(),)
        super(Hashtag, self).save(*args, **kwargs)   


@python_2_unicode_compatible
class Picture(models.Model):

    identifier = models.CharField(
        editable=False,
        max_length=300,
        null=False,
        blank=False,
        unique=True
    )
    image = models.ImageField(
        verbose_name="Picture",
        upload_to=get_file_name,
        null=True,
        blank=True
    )
    image_with_filter = models.ImageField(
        verbose_name="Picture with filter",
        upload_to=get_file_name,
        null=True,
        blank=True
    )
    title = models.CharField(_('Title of picture'), blank=True, max_length=255)
    public = models.BooleanField(default=True)
    user = models.ForeignKey('users.User', null=True, blank=True,
            verbose_name='User',related_name='user_picture',
            on_delete=models.SET_NULL)
    album = models.ForeignKey(Album, null=True, blank=True,
            verbose_name='Album',related_name='picture_album',
            on_delete=models.SET_NULL)
    address_es = models.CharField(_('Address ES'), blank=True, max_length=255)
    address_it = models.CharField(_('Address IT'), blank=True, max_length=255)
    address_fr = models.CharField(_('Address FR'), blank=True, max_length=255)
    address_en = models.CharField(_('Address EN'), blank=True, max_length=255)        
    picture_filter = models.CharField(_('Filter'), blank=True, max_length=255)      

    # Disabled, pending of Google Maps
    '''
    country = models.ForeignKey('locations.Country', null=True, blank=True,
            verbose_name='Country',related_name='country_picture',
            on_delete=models.SET_NULL)
    '''
    # Meta info 
    meta_location = geomodels.PointField(null=True, blank=True, spatial_index=True, geography=True)     
    meta_date = models.DateTimeField(auto_now_add=False, blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # Hashtags
    hashtags = models.ManyToManyField(Hashtag,null=True, blank=True)

    # For rename images
    directory_string_var = 'pictures'    

    reported = models.BooleanField(default=False)
    hide = models.BooleanField(default=False)

    order = models.IntegerField(
        verbose_name="Order",
        null=True,
        default=0        
    )
    __original_filter = None

    def __init__(self, *args, **kwargs):
        super(Picture, self).__init__(*args, **kwargs)
        self.__original_filter = self.picture_filter


    @property
    def my_item(self):
        """
        The purpose of this property is create a related object before the parent is created. 
        This property is only used on the "create" serializer event.
        """
        if self.album is None:
            return create_related_object(self,object_type='picture')
        else:
            return False

    @property
    def url(self):
        if self.image:
            url = get_thumbnailer(self.image)['avatar_mini'].url
            return url
        else:
            return None

    def __str__(self):
        if self.user is not None:
            return 'Picture {0} - {1}'.format(self.identifier, self.user.username)  
        else:
            return 'Picture {0}'.format(self.identifier)  

    def save(self, *args, **kwargs):
        if not self.id:
            self.identifier = "%s" % (uuid.uuid4(),)
            if self.picture_filter:
                # lauch image processing on start
                apply_filters(self.identifier)
            location_notification(self.identifier)

        if self.picture_filter != self.__original_filter:
            # lauch image processing on update
            apply_filters(self.identifier)
            
        super(Picture, self).save(*args, **kwargs)


@python_2_unicode_compatible
class Usertag(models.Model):
    identifier = models.CharField(
        editable=False,
        max_length=300,
        null=False,
        blank=False,
        unique=True
    )

    user = models.ForeignKey('users.User', null=True, blank=True,
            verbose_name='User',related_name='usertag_user',
            on_delete=models.SET_NULL)
    non_user = models.CharField(_('Not user username'), blank=True, max_length=20)      
    owner = models.ForeignKey('users.User', null=True, blank=True,
            verbose_name='User Owner',related_name='usertag_owner',
            on_delete=models.SET_NULL)            
    picture = models.ForeignKey(Picture, null=True, blank=True,
            verbose_name='Picture',related_name='usertag_picture',
            on_delete=models.SET_NULL)        
    top = models.IntegerField(
        verbose_name="Top Coords",
        null=False,
        default=0        
    )
    left = models.IntegerField(
        verbose_name="Left coords",
        null=False,
        default=0        
    )

    active = models.BooleanField(default=True)
    hide = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # class Meta:
    # unique_together = (("user", "picture"),)

    def save(self, *args, **kwargs):
        if not self.id:
            self.identifier = "%s" % (uuid.uuid4(),)
            
        super(Usertag, self).save(*args, **kwargs)    


@python_2_unicode_compatible
class UserComment(models.Model):
    identifier = models.CharField(
        editable=False,
        max_length=300,
        null=False,
        blank=False,
        unique=True
    )

    user = models.ForeignKey('users.User', null=True, blank=True,
            verbose_name='User',related_name='comment_user',
            on_delete=models.SET_NULL)
    picture = models.ForeignKey(Picture, null=True, blank=True,
            verbose_name='Picture',related_name='comment_picture',
            on_delete=models.SET_NULL)
    comment = models.CharField(
        editable=True,
        max_length=300,
        null=False,
        blank=False,
        unique=False
    )
    hide = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)    

    def save(self, *args, **kwargs):
        if not self.id:
            self.identifier = "%s" % (uuid.uuid4(),)
        super(UserComment, self).save(*args, **kwargs)    


@python_2_unicode_compatible
class MediaItem(models.Model):
    """
    We use this model to encapsule all media items. 
    In our timeline we get a queryset of media items, albums and pictures.
    """
    identifier = models.CharField(
        editable=False,
        max_length=300,
        null=False,
        blank=False,
        unique=True
    )

    picture = models.ForeignKey(Picture, null=True, blank=True,
            verbose_name='Picture',related_name='item_picture',
            on_delete=models.SET_NULL)

    album = models.ForeignKey(Album, null=True, blank=True,
            verbose_name='Album',related_name='item_album',
            on_delete=models.SET_NULL)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def main_image(self):
        if self.picture:
            return self.picture.identifier
        if self.album:
            picture = Picture.objects.filter(album=self.album)[1]
            if picture:
                return picture.identifier
        return None

    def __str__(self):
        if self.picture is not None:
            return 'Picture {0} - {1}'.format(self.picture.identifier, self.picture.user.username)  
        else:
            return 'Album {0} - {1}'.format(self.album.identifier, self.album.user.username)  

    def save(self, *args, **kwargs):
        if not self.id:
            self.identifier = "%s" % (uuid.uuid4(),)
        super(MediaItem, self).save(*args, **kwargs)


@python_2_unicode_compatible
class ReportItem(models.Model):
    """
    We use this model to encapsule all media items. 
    In our timeline we get a queryset of media items, albums and pictures.
    """
    identifier = models.CharField(
        editable=False,
        max_length=300,
        null=False,
        blank=False,
        unique=True
    )

    picture = models.ForeignKey(Picture, null=True, blank=True,
            verbose_name='Picture',related_name='report_picture',
            on_delete=models.SET_NULL)

    user = models.ForeignKey('users.User', null=True, blank=True,
            verbose_name='User',related_name='report_user',
            on_delete=models.SET_NULL)

    comment = models.CharField(
        editable=True,
        max_length=300,
        null=False,
        blank=False,
        unique=False,
        default=''
    )

    checked = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        if self.picture:
            picture_info = self.picture.identifier
        else:
            picture_info  = "No pic."
        if self.user:
            user_info = self.user.username
        else:
            user_info  = "No user."
        
        return 'Report Picture {0} - by user {1}'.format(picture_info, user_info)  


    def save(self, *args, **kwargs):
        if not self.id:
            self.identifier = "%s" % (uuid.uuid4(),)
        # When we add a new report, we need to set a picture as reported. 
        if self.picture and self.checked == False:
            picture = Picture.objects.filter(pk=self.picture.pk).update(reported=True)
            # Must we need to send a message to admin to check this picture.

        elif self.checked == True:
            num_items = ReportItem.objects.filter(picture=self.picture,checked=False).count()
            if num_items == 0:
                picture = Picture.objects.filter(pk=self.picture.pk).update(reported=False)

        super(ReportItem, self).save(*args, **kwargs)



def save_picture(sender, instance, **kwargs):
    '''
    We update the associated address.
    '''
    try:
        picture = Picture.objects.get(pk=instance.pk)
    except Picture.DoesNotExist:
        picture = None

    if instance.meta_location and instance.address_en == '':
        lat = instance.meta_location.y
        lon = instance.meta_location.x
        address = get_full_address(lat,lon)
        instance.address_en = address
        instance.address_es = address
        instance.address_fr = address
        instance.address_it = address
    else:
        #If we have changed the coords
        if picture is not None and picture.meta_location != instance.meta_location:    
            lat = instance.meta_location.y
            lon = instance.meta_location.x
            address = get_full_address(lat,lon)
            instance.address_en = address
            instance.address_es = address
            instance.address_fr = address
            instance.address_it = address

pre_save.connect(save_picture, sender=Picture)

def save_album(sender, instance, **kwargs):
    if instance.meta_location and instance.address_en == '':
        lat = instance.meta_location.y
        lon = instance.meta_location.x
        address = get_full_address(lat,lon)
        instance.address_en = address
        instance.address_es = address
        instance.address_fr = address
        instance.address_it = address
        instance.save()

post_save.connect(save_album, sender=Album)
