from django import forms
from django.contrib import admin
from .models import NotificationLibrary, NotificationItem, UserDevices
from ckeditor_uploader.widgets import CKEditorUploadingWidget


@admin.register(NotificationLibrary)
class NotificationLibrarysAdmin(admin.ModelAdmin):
    list_display = ('id', 'key', 'notification_type',
                    'text_en', 'push', 'email', 'active')
    search_fields = ['key', 'text']
    list_filter = ['active', 'notification_type']


@admin.register(NotificationItem)
class NotificationItemsAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'notification_library',
                    'status', 'created_at')
    search_fields = ['user__first_name',
                     'notification_library__key', 'created_at']
    list_filter = ['status']
    ordering = ['-id']


@admin.register(UserDevices)
class UserDevicesAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'player_id',
                    'active', 'created_at', 'updated_at')
    list_filter = ['active']
    ordering = ['-id']

