EMAIL_TEMPLATES = {
    'EN': {
        'VERIFICATION_ACCOUNT': {
            'TEMPLATE_ID': 'd-14af651e335343d3938ba22297c3aff4',
            'SUBJECT': 'Verify your email address!',
            'WELCOME': 'Welcome in Facebusters, {{username}}!',
            'CONTENT': 'You’re receiving this email because your registration in Facebusters. Use this code to activate your account and start to catch your face: {{verification_code}} '
        },
        'ACCOUNT_VERIFIED': {
            'TEMPLATE_ID': 'd-722e63c3e4d34e129a1390606c74a118',
            'SUBJECT': 'Email address verified',
            'WELCOME': 'You are a Facebuster now, {{username}}!',
            'CONTENT': 'Log in and start to catch your face!'
        },
        'RESET_PASSWORD': {
            'TEMPLATE_ID': 'd-8414899eaa0d424a82386a90cfc83354',
            'SUBJECT': 'Reset your password',
            'WELCOME': 'Do you want to reset your password, {{username}}?',
            'CONTENT': 'Use this code in the Forgot Password app page to choose your new password: {{password}}'
        },
        'PASSWORD_IS_CHANGED': {
            'TEMPLATE_ID': 'd-a86ed2e5667148b7b4ee471e0a089a6c',
            'SUBJECT': 'Your password has been changed',
            'WELCOME': 'Hi, {{username}}!',
            'CONTENT': 'Your password has been successfully updated. Log in now and start to catch your face!'
        }
    },
    'IT': {
        'VERIFICATION_ACCOUNT': {
            'TEMPLATE_ID': 'd-14af651e335343d3938ba22297c3aff4',
            'SUBJECT': 'Verifica il tuo indirizzo email!',
            'WELCOME': 'Benvenut@ su Facebusters, {{username}}!',
            'CONTENT': 'Usa questo codice per verificare il tuo indirizzo email e iniziare subito a cercare il tuo viso: {{verification_code}} '
        },
        'ACCOUNT_VERIFIED': {
            'TEMPLATE_ID': 'd-722e63c3e4d34e129a1390606c74a118',
            'SUBJECT': 'Indirizzo email verificato',
            'WELCOME': 'Sei un Facebuster adesso, {{username}}!',
            'CONTENT': 'Fai l accesso e inizia subito a cercare la tua faccia nelle foto della community!'
        },
        'RESET_PASSWORD': {
            'TEMPLATE_ID': 'd-8414899eaa0d424a82386a90cfc83354',
            'SUBJECT': 'Reimposta la tua password',
            'WELCOME': 'Vuoi reimpostare la tua password, {{username}}?',
            'CONTENT': 'Utilizza questo codice nella app per scegliere la tua nuova password: {{password}}'
        },
        'PASSWORD_IS_CHANGED': {
            'TEMPLATE_ID': 'd-a86ed2e5667148b7b4ee471e0a089a6c',
            'SUBJECT': 'Passord modificata',
            'WELCOME': 'Ciao, {{username}}!',
            'CONTENT': 'La tua password è stata modificata con successo. Fai l accesso e inizia subito a cercare la tua faccia nelle foto della community!'
        }
    },
    'ES': {
        'VERIFICATION_ACCOUNT': {
            'TEMPLATE_ID': 'd-14af651e335343d3938ba22297c3aff4',
            'SUBJECT': 'Verifica tu dirección de correo electrónico!',
            'WELCOME': 'Bienvenido a Facebusters, {{username}}!',
            'CONTENT': 'Si has recibido este email es por que te has registrado en Facebusters. Usa este código para activar tu cuenta: {{verification_code}} '
        },
        'ACCOUNT_VERIFIED': {
            'TEMPLATE_ID': 'd-14af651e335343d3938ba22297c3aff4',
            'SUBJECT': 'Cuenta verificada correctamente!',
            'WELCOME': 'Bienvenido a Facebusters, {{username}}!',
        },
        'RESET_PASSWORD': {
            'TEMPLATE_ID': 'd-8414899eaa0d424a82386a90cfc83354',
            'SUBJECT': 'Recuerda tu contraseña',
            'WELCOME': 'Quieres resetear tu contraseña, {{username}}?',
            'CONTENT': 'Utiliza el siguiente código para reestablecer tu contraseña: {{password}}'
        },
        'PASSWORD_IS_CHANGED': {
            'TEMPLATE_ID': 'd-a86ed2e5667148b7b4ee471e0a089a6c',
            'SUBJECT': 'Contraseña modificada',
            'WELCOME': 'Hasta pronto, {{username}}!',
            'CONTENT': 'Tu contrraseña se ha modificado con exito. Inicia sesión y empieza a agregar contactos!'
        }
    }
}
