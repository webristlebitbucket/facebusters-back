from rest_framework import serializers
from facebusters_project.notifications.models import NotificationLibrary, NotificationItem
from facebusters_project.pictures.models import Picture
from easy_thumbnails.files import get_thumbnailer

from datetime import date, datetime


class NotificationLibrarySerializer(serializers.ModelSerializer):
    notification_type = serializers.CharField(
        source='get_notification_type_display')

    class Meta:
        model = NotificationLibrary
        fields = ('key', 'text_en','text_es','text_it','text_fr', 'notification_type',
                  'linked_view')


class NotificationItemSerializer(serializers.ModelSerializer):
    get_notification_info = serializers.SerializerMethodField(
        'notification_info')

    get_picture_info = serializers.SerializerMethodField(
        'picture_info')


    def notification_info(self, obj):
        return NotificationLibrarySerializer(obj.notification_library, many=False).data

    def picture_info(self, obj):
        if obj.related_item_key:
            # Return related picture URL
            # Check if already exists a picture with this ID    
            try:
                picture = Picture.objects.get(identifier=obj.related_item_key)
            except Picture.DoesNotExist:
                return None

            if picture is not None and picture.image is not None:
                image_url = get_thumbnailer(picture.image)['avatar_mini'].url
                if image_url is not None:
                    image_url = self.context.get("request").build_absolute_uri(image_url)
                else:
                    image_url = None
                return image_url
            else:
                return None

        return NotificationLibrarySerializer(obj.notification_library, many=False).data

    class Meta:
        model = NotificationItem
        fields = ('id', 'status', 'created_at', 'get_notification_info', 'get_picture_info', 'related_item_key')
