from django.db import models


class ActiveNotificationsManager(models.Manager):
    def get_queryset(self):
        return super(ActiveNotificationsManager, self).get_queryset().exclude(active=False)
