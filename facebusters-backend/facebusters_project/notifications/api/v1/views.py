from django.core.urlresolvers import reverse
from rest_framework.response import Response
from rest_framework import viewsets, pagination
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.views import APIView
from facebusters_project.utils.code_responses import get_response
from facebusters_project.notifications.models import NotificationItem, NotificationLibrary, UserDevices
from .serializers import NotificationItemSerializer
from facebusters_project.users.models import User
from rest_framework import viewsets, pagination, mixins


class NotificationCounterApiView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        if not request.user:
            response = get_response('USER_NOT_EXISTS')
            return Response(data=response['data'], status=response['status'])
        else:
            received = request.data
            notifications = NotificationItem.objects.filter(
                status='received', user=request.user).count()
            return Response(data=({'notification': notifications}))


class NotificationBulkUpdateApiView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        if not request.user:
            response = get_response('USER_NOT_EXISTS')
            return Response(data=response['data'], status=response['status'])
        else:
            received = request.data
            status_list = ['deleted', 'readed']
            if 'status' in received.keys() and received['status'] in status_list:
                notification = NotificationItem.objects.filter(
                    user=request.user).exclude(status='deleted').update(status=received['status'])
                response = get_response('NOTIFICATION_UPDATED_OK')
                return Response(data=response['data'], status=response['status'])
            else:
                response = get_response('FIELDS_MISSING')
                return Response(data=response['data'], status=response['status'])


class NotificationViewSet(mixins.ListModelMixin,
                          mixins.RetrieveModelMixin,
                          viewsets.GenericViewSet):
    """
    List notifications
    """
    http_method_names = ['get', 'list', 'patch']
    model_class = NotificationItem
    queryset = NotificationItem.objects.exclude(
        status='deleted').order_by('-id')
    serializer_class = NotificationItemSerializer
    paginate_by = 20
    lookup_fields = ('id')

    def get_serializer_context(self):
        return {'request': self.request}

    def list(self, request):
        queryset = self.queryset.filter(user=self.request.user)
        query = self.request.query_params
        if 'received' in query.keys():
            queryset = queryset.filter(status='received')
        if 'readed' in query.keys():
            queryset = queryset.filter(status='readed')
        if 'notreaded' in query.keys():
            queryset = queryset.exclude(status='readed')
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = NotificationItemSerializer(
                page, many=True, context={'user': self.request.user, "request": self.request})
            return self.get_paginated_response(serializer.data)
        serializer = NotificationItemSerializer(
            data=queryset,  many=True)
        serializer.is_valid(raise_exception=False)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        received = request.data
        status_list = ['deleted', 'readed']
        if 'status' in received.keys() and received['status'] in status_list:
            try:
                notification = self.queryset.get(
                    user=self.request.user, id=pk)
                notification.status = received['status']
                notification.save()
                response = get_response('NOTIFICATION_UPDATED_OK')
                return Response(data=response['data'], status=response['status'])
            except self.model_class.DoesNotExist:
                response = get_response('NOTIFICATION_NOT_EXISTS')
                return Response(data=response['data'], status=response['status'])
        else:
            response = get_response('FIELDS_MISSING')
            return Response(data=response['data'], status=response['status'])


class UserDeviceApiView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        if not request.user:
            response = get_response('USER_NOT_EXISTS')
            return Response(data=response['data'], status=response['status'])
        else:
            received = request.data
            
            if 'status' in received.keys() and 'player_id' in received.keys():
                # Check if device exists
                try:
                    device = UserDevices.objects.get(player_id=received['player_id'],active=True)
                    
                except UserDevices.DoesNotExist:
                    device = None

                if received['status'] == 'add':
                    if device is not None:
                        response = get_response('DEVICE_ALREADY_EXISTS')
                        return Response(data=response['data'], status=response['status'])
                    else:
                        device = UserDevices()
                        device.user=request.user
                        device.player_id=received['player_id']
                        device.active=True
                        device.save()

                        response = get_response('DEVICE_ADDED')
                        return Response(data=response['data'], status=response['status'])

                if received['status'] == 'remove':
                    if device is not None:
                        device.active=False
                        device.save()

                        response = get_response('DEVICE_REMOVED')
                        return Response(data=response['data'], status=response['status'])
                    else:
                        response = get_response('DEVICE_NOT_EXISTS')
                        return Response(data=response['data'], status=response['status'])

                response = get_response('NOTIFICATION_UPDATED_OK')
                return Response(data=response['data'], status=response['status'])
            else:
                response = get_response('FIELDS_MISSING')
                return Response(data=response['data'], status=response['status'])