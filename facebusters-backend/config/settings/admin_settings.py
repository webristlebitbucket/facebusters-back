from django.utils.translation import ugettext_lazy as _
# BATON CONFIGURATION
BATON = {
    'SITE_HEADER': 'Facebusters',
    'SITE_TITLE': 'Facebusters',
    'INDEX_TITLE': 'Admin Panel',
    'SUPPORT_HREF': '',
    'COPYRIGHT': 'copyright © 2018 Facebusters',  # noqa
    'POWERED_BY': 'Facebusters',
    'CONFIRM_UNSAVED_CHANGES': True,
    'MENU': (
        {'type': 'title', 'label': 'main', 'apps': ('users')},
        {
            'type': 'app',
            'name': 'users',
            'label': 'Users administration',
            'icon': 'fa fa-lock',
            'models': (
                {
                    'name': 'user',
                    'label': 'Users'
                },
                {
                    'name': 'userfollow',
                    'label': 'User relations'
                },
            )
        },
        {
            'type': 'app',
            'name': 'pictures',
            'label': 'Picture administration',
            'icon': 'fa fa-lock',
            'models': (
                {
                    'name': 'picture',
                    'label': 'Pictures'
                },
                {
                    'name': 'usertag',
                    'label': 'User Tags'
                },
                {
                    'name': 'hashtag',
                    'label': 'Hashtags'
                },                
                {
                    'name': 'usercomment',
                    'label': 'Comments'
                },          
                {
                    'name': 'album',
                    'label': 'Albums'
                },
                {
                    'name': 'reportitem',
                    'label': 'Reported pictures'
                },                                              
            )
        },
        {
            'type': 'app',
            'name': 'notifications',
            'label': 'Gestión de notificaciones',
            'icon': 'fa fa-envelope-o',
            'models': (
                {
                    'name': 'notificationlibrary',
                    'label': 'Notifications'
                },
                {
                    'name': 'notificationitem',
                    'label': 'Notification users'
                },
            )
        },
    )
}
