# using SendGrid's Python Library
# https://github.com/sendgrid/sendgrid-python
import sendgrid
import os
from sendgrid.helpers.mail import *
from .email_templates import EMAIL_TEMPLATES

def replaceEmailTemplateTextKeys(template, key, values ,language='EN'):
    '''
        Return text with texts are replaced by values of dict on any key of template
        template ==> "VERIFICATION_ACCOUNT"
        key      ==> "content"
        values   ==> {'username': eric_dev, 'verification_code': 01235}
        return   ==> "Welcome eric_dev ..... activate account: 012345"
    '''
    t = EMAIL_TEMPLATES[language][template][key]
    for k, v in values.items():
        t = t.replace('{{'+str(k)+'}}', str(v))
    return t

def sendEmail(to_email, template, values, language='EN'):
    ''' 
        to_email ==> email to send
        template ==> text to use of dict
        values   ==> dict to text to replace on template
    '''
    from_email = Email(os.environ.get('SENDGRID_EMAIL_SENDER'))

    # Get strings to substitute
    template_id = EMAIL_TEMPLATES[language][template]['TEMPLATE_ID']
    subject_text = EMAIL_TEMPLATES[language][template]['SUBJECT']
    welcome_text = replaceEmailTemplateTextKeys(template, 'WELCOME', values)
    content_text = replaceEmailTemplateTextKeys(template, 'CONTENT', values)
    try:
        sg = sendgrid.SendGridAPIClient(apikey=os.environ.get('SENDGRID_API_KEY'))
        content = Content("text/html", " ")
        mail = {
            'from': {
                'email': 'no-reply@facebusters.com'
            },
            'subject': str(subject_text), 
            'template_id': str(template_id), 
            'content': [
                {'value': ' ', 'type': 'text/html'}
            ], 
            'personalizations': [
                {
                    "dynamic_template_data": {
                        "SUBJECT": subject_text,
                        "WELCOME": welcome_text,
                        "CONTENT": content_text
                    },
                    'to': [
                        {
                            'email': str(to_email)
                        }
                    ]
                }
            ]
        }
        response = sg.client.mail.send.post(request_body=mail)
    except Exception as error:
        print('Error sending mail')
        print(error)
        return False
    return True
    