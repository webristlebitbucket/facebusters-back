from __future__ import unicode_literals
from rest_framework import serializers
from rest_framework.views import APIView
from facebusters_project.users.models import User, UserFollow
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.authentication import TokenAuthentication
from utils.code_responses import get_response
from easy_thumbnails.files import get_thumbnailer
from drf_extra_fields.fields import Base64ImageField
from facebusters_project.users.models import UserFollow

class UserSerializer(serializers.ModelSerializer):
    avatar_thumbnail = serializers.SerializerMethodField('avatar_thumb')
    avatar = Base64ImageField()

    def image_thumb(self, obj):
        if obj.avatar:
            if obj.avatar is not None:
                image_url = get_thumbnailer(obj.avatar)['stream_picture'].url
                if image_url is not None:
                    image_url = self.context.get("request").build_absolute_uri(image_url)
                else:
                    image_url = None
            else:
                image_url = None
        else:
            image_url = None
        return image_url


    def avatar_thumb(self, obj):
        if obj.avatar:
            print('AVATAR ONE')
            print(obj.avatar)
            print(type(obj.avatar))
            if obj.avatar is not None:
                image_url = get_thumbnailer(obj.avatar)['stream_picture'].url
                if image_url is not None:
                    image_url = self.context.get("request").build_absolute_uri(image_url)
                else:
                    image_url = None
            else:
                image_url = None
        else:
            image_url = None
        return image_url

    class Meta:
        model = User
        fields = ('id','identifier', 'first_name', 'last_name','email','num_pictures', 'num_albums', 'avatar',
            'num_tagged_in','num_followers', 'num_following', 'num_connections', 'avatar', 'avatar_thumbnail', 'username',
            'language', 'delete_account', 'tag_control')
        read_only_fields = ('id', 'identifier', 'username')


class UserMinSerializer(serializers.ModelSerializer):
    avatar_thumbnail = serializers.SerializerMethodField('avatar_thumb')

    def avatar_thumb(self, obj):
        request = self.context.get('request')
        if obj.avatar:
            avatar_url = get_thumbnailer(obj.avatar)['avatar'].url
            try:
                avatar_url = request.build_absolute_uri(avatar_url)
            except Exception as error:
                avatar_url = get_thumbnailer(obj.avatar)['avatar'].url
        else:
            avatar_url = None
        return avatar_url

    class Meta:
        model = User
        fields = ('identifier', 'username', 'avatar_thumbnail')


class UserFollowSerializer(serializers.ModelSerializer):
    user_info = serializers.SerializerMethodField('user_data')
    follow_to_info = serializers.SerializerMethodField('follow_to_data')
    follow_back = serializers.SerializerMethodField('follow_back_data')

    def user_data(self, obj):
        if obj.user:
            serializer = UserMinSerializer(
                obj.user, many=False, read_only=True,
                context={"request": self.context.get("request")})
            return serializer.data
        else:
            return None

    def follow_to_data(self, obj):
        if obj.follow_to:
            serializer = UserMinSerializer(
                obj.follow_to, many=False, read_only=True,
                context={"request": self.context.get("request")})
            return serializer.data
        else:
            return None

    def follow_back_data(self, obj):
        if obj.follow_to:
            # We need to check if one user follow each other
            try:
                follow_back = UserFollow.objects.get(user=obj.follow_to, follow_to=obj.user,active=True)
                return True
            except UserFollow.DoesNotExist:
                return False

    class Meta:
        model = UserFollow
        fields = ('user_info', 'follow_to_info', 'follow_back', 'fb_follow')


class UserPublicSerializer(serializers.ModelSerializer):
    avatar_thumbnail = serializers.SerializerMethodField('avatar_thumb')
    following = serializers.SerializerMethodField('following_data')

    def avatar_thumb(self, obj):
        request = self.context.get('request')
        if obj.avatar:
            avatar_url = get_thumbnailer(obj.avatar)['avatar'].url
            try:
                avatar_url = request.build_absolute_uri(avatar_url)
            except Exception as error:
                avatar_url = get_thumbnailer(obj.avatar)['avatar'].url
        else:
            avatar_url = None
        return avatar_url

    def following_data(self, obj):
        request = self.context.get("request")
        user = request.user
        if obj.pk in user.list_following:
            return True
        else:
            return False


    class Meta:
        model = User
        fields = ('identifier', 'first_name', 'last_name','email','num_pictures', 'num_albums',
            'num_tagged_in','num_followers', 'num_following', 'num_connections', 'avatar_thumbnail', 'username','following')

class UserSearchSerializer(serializers.ModelSerializer):
    avatar_thumbnail = serializers.SerializerMethodField('avatar_thumb')
    connection = serializers.SerializerMethodField('user_connection')

    def avatar_thumb(self, obj):
        request = self.context.get('request')
        if obj.avatar:
            avatar_url = get_thumbnailer(obj.avatar)['avatar'].url
            try:
                avatar_url = request.build_absolute_uri(avatar_url)
            except Exception as error:
                avatar_url = get_thumbnailer(obj.avatar)['avatar'].url
        else:
            avatar_url = None
        return avatar_url

    def user_connection(self, obj):
        request = self.context.get('request')

        check_user_follow_to = UserFollow.objects.filter(user=obj, follow_to=request.user, active=True).count()
        check_follow_to_user = UserFollow.objects.filter(user=request.user, follow_to=obj, active=True).count()
        if (check_user_follow_to < 1 or check_follow_to_user < 1):
            connection = False
        else:
            connection = True

        return connection

    class Meta:
        model = User
        fields = ('identifier', 'first_name', 'last_name', 'avatar_thumbnail', 'username', 'connection')
