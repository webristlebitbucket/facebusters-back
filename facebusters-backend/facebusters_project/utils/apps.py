from django.apps import AppConfig


class UtilsConfig(AppConfig):
    name = 'facebusters_project.utils'
    verbose_name = "Utils"
