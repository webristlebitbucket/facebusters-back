# Rest framework imports
from rest_framework import viewsets, pagination, mixins
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

# Django Core imports
from django.core.exceptions import PermissionDenied
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.views.generic import DetailView, ListView, RedirectView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q

# Models imports
from facebusters_project.pictures.models import (
    Picture, 
    Album, 
    UserComment, 
    Usertag, 
    Hashtag,
    MediaItem,
    ReportItem
)
from facebusters_project.users.models import (
    User,
    UserFollow
)

# Serializers imports
from .serializers import (
    PictureSerializer,
    CreatePictureSerializer,
    UserCommentSerializer, 
    AlbumSerializer, 
    CreateAlbumSerializer,
    HashtagSerializer, 
    UsertagSerializer,
    MediaItemSerializer,
    ReportItemSerializer,
    SmallPictureSerializer
)

# Utils imports
from facebusters_project.utils.code_responses import get_response
from facebusters_project.utils.utils import code_generator
from facebusters_project.notifications.utils import send_notification_tag

#GIS Imports
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import Distance  
from facebusters_project.utils.utils import get_full_address, get_address_google, get_address_place_id

#System imports
import json

#Rest test imports
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser


class PictureViewSet(viewsets.ModelViewSet):
    http_method_names = ['retrieve', 'get', 'create','post', 'patch']
    model_class = Picture
    queryset = Picture.objects.all()
    serializer_class = PictureSerializer
    paginate_by = 20
    lookup_fields = ('identifier')

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return CreatePictureSerializer
        if self.request.method == 'GET':
            query = self.request.query_params
            if 'map' in query:
                return SmallPictureSerializer
        return PictureSerializer

    def get_serializer_context(self):
        return {'request': self.request}

    def get_permissions(self):
        self.permission_classes = [IsAuthenticated, ]
        return super(PictureViewSet, self).get_permissions()

    def retrieve(self, request, pk=None):
        instance = self.model_class.objects.filter(
            **{self.lookup_fields: pk})
        if len(instance) == 1:
            serializer = self.get_serializer(instance[0])
            return Response(serializer.data)
        else:
            response = get_response('PICTURE_NOT_EXISTS')
            return Response(data=response['data'], status=response['status'])
    
    def partial_update(self, request, pk=None): 
        instance = self.model_class.objects.get(**{self.lookup_fields: pk})
        if request.user != instance.user or request.user.delete_account == True:
            raise PermissionDenied

        serializer = self.serializer_class(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    def get_queryset(self):
        """
        This view should return a list pictures. If return the logged user, return all pictures. 
        If return another user, only public pictures.
        """
        
        query = self.request.query_params
        queryset = self.queryset
        if 'search' in query:
            query_terms=0
            if 'lat' in query and 'lon' in query:
                query_terms=1
                if 'distance' in query:
                    distance = int(query['distance'])
                else:
                    distance = 1

                lon = float(query['lon'])
                print(lon)
                lat = float(query['lat'])
                print(lat)
                point = Point(lon, lat)
                
                queryset = self.queryset.filter(meta_location__distance_lt=(point, Distance(km=distance)),hide=False)
            if 'startdate' in query and 'enddate' in query:
                query_terms=1
                # Maybe we need to check this field format.
                startdate = query['startdate']
                enddate = query['enddate']

                queryset = queryset.filter(meta_date__gt=startdate, meta_date__lt=enddate)
    
            if 'author' in query:
                query_terms=1
                author = query['author']
                try:
                    author = User.objects.get(identifier=query['author'])
                    queryset = queryset.filter(user=author)
                except User.DoesNotExist:
                    pass

            if 'hashtags' in query:
                query_terms=1
                hashtags = query['hashtags'].split(',')
                queryset = queryset.filter(hashtags__in=hashtags).distinct()

            if 'address' in query:
                if query['address'] == '' and query_terms==0:
                    queryset = Picture.objects.none()

            queryset = queryset.exclude(Q(album__private=True) |  Q(hide=True)|  Q(public=False))

        else:
            if 'user' in query:
                queryset = queryset.filter(user__identifier=query['user'], public=True,hide=False)
            elif 'album' in query:
                queryset = queryset.filter(album__identifier=query['album'],hide=False)
            elif 'all' in query:
                queryset = queryset.filter(hide=False)
            elif 'public' in query: 
                print('ok publico')
                queryset = queryset.filter(user=self.request.user, public=True, hide=False)
            elif 'private' in query:
                print('ok private')
                queryset = queryset.filter(user=self.request.user, public=False, hide=False)
            else:
                queryset = queryset.filter(user=self.request.user,hide=False)

            if 'tagged_in' in query:
                if 'user' in query:
                    tags = Usertag.objects.filter(user__identifier=query['user'],hide=False,active=True).values_list('picture__pk')

                    # We need to hide pictures when are private and im not the owner.
                    queryset = Picture.objects.all().filter(id__in=tags).exclude(
                        (~Q(user = self.request.user) & Q(public = False))
                        )

                    # Filter again to exclude private albums
                    queryset = queryset.exclude(
                        (~Q(user = self.request.user) & Q(album__private=True))
                        )                 
                    queryset = queryset.exclude(hide=True)
                    
                else:
                    tags = Usertag.objects.filter(user=self.request.user,hide=False,active=True).values_list('picture__pk')
                    queryset = Picture.objects.all().filter(id__in=tags) # FIX ME, filter by active and public pictures... 

        return queryset.order_by('-created_at').exclude(user__delete_account=True)


class CommentViewSet(viewsets.ModelViewSet):
    http_method_names = ['retrieve', 'get','post']
    model_class = UserComment
    queryset = UserComment.objects.all()
    serializer_class = UserCommentSerializer
    paginate_by = 20
    lookup_fields = ('identifier')

    def get_permissions(self):
        self.permission_classes = [IsAuthenticated, ]
        return super(CommentViewSet, self).get_permissions()

    def get_queryset(self):
        """
        This view should return a list pictures. If return the logged user, return all pictures. 
        If return another user, only public pictures.
        """
        query = self.request.query_params
        queryset = self.queryset

        if 'picture' in query:
            queryset = self.queryset.filter(picture__identifier=query['picture']).order_by('-created_at')
        else:
            queryset = UserComment.objects.none()

        return queryset.exclude(user__delete_account=True)

    def create(self, request, format=None):
        '''
        On this view you can:
            - Add new comment
            
        We receive:
        {
            comment:"Loren ipsum dolor sit amet.."
            picture: IDENTIFIER
        }
        '''

        if not request.user:
            response = get_response('USER_NOT_EXISTS')
            return Response(data=response['data'], status=response['status'])
        
        info = request.data


        if 'comment' in info and 'picture' in info:
            try:
                print(info['picture'])
                picture = Picture.objects.get(identifier=info['picture'])
            except Picture.DoesNotExist:
                picture = None
                response = get_response('PICTURE_NOT_EXISTS')
                return Response(data=response['data'], status=response['status'])

            if picture is not None:
                new_comment= UserComment()
                new_comment.user = self.request.user
                new_comment.comment = info['comment']
                new_comment.picture = picture
                new_comment.save()

                response = get_response('COMMENT_CREATED')
                return Response(data=response['data'], status=response['status'])     
        else:
            response = get_response('REQUIRED_FIELDS')
            return Response(data=response['data'], status=response['status'])     


class AlbumViewSet(viewsets.ModelViewSet):
    http_method_names = ['retrieve', 'get','create','post','patch']
    model_class = Album
    queryset = Album.objects.all()
    serializer_class = AlbumSerializer
    paginate_by = 20
    lookup_fields = ('identifier')       

    def get_permissions(self):
        self.permission_classes = [IsAuthenticated, ]
        return super(AlbumViewSet, self).get_permissions()

    def get_queryset(self):
        """
        This view should return a list pictures. If return the logged user, return all pictures. 
        If return another user, only public pictures.
        """
        query = self.request.query_params
        queryset = self.queryset

        if 'user' in query:
            queryset = self.queryset.filter(user__identifier=query['user'],hide=False,private=False)
        else:
            if self.request.method == 'GET':
                #Filter for get list. 
                if 'pk' in self.kwargs:
                    # Only return private album to owner.
                    try:
                        album = Album.objects.get(pk=self.kwargs['pk'])
                        if album.private == True:
                            if album.user != self.request.user:
                                queryset = Album.objects.none()
                            else:
                                queryset = self.queryset.filter(hide=False)
                        else:
                            queryset = self.queryset.filter(hide=False,private=False)
                    except Album.DoesNotExist:
                        queryset = Album.objects.none()
                else:
                    queryset = self.queryset.filter(user=self.request.user,hide=False)

        if 'private' in query:
            queryset = queryset.filter(private=query['private'])

        return queryset.order_by('-created_at').exclude(user__delete_account=True)

    def create(self, request, format=None):
        '''
        On this view you can:
            - Add new comment
            
        We receive:
        {
            "meta_location":{
                "latitude": 37.0625,
                "longitude": -95.677068
            },
            "title":"El title",
        }
        '''

        if not request.user:
            response = get_response('USER_NOT_EXISTS')
            return Response(data=response['data'], status=response['status'])

        
        info = request.data

        if 'meta_location' in info and 'title' in info and 'private' in info:
            new_album={}
            new_album['title'] = info['title']
            new_album['user'] = self.request.user.pk
            new_album['meta_location'] = info['meta_location']
            new_album['private'] = info['private']

            serializer = CreateAlbumSerializer(data=new_album, partial=False, many=False, context={'user': self.request.user, "request": self.request})

            if serializer.is_valid():
                serializer.save()
            else:
                print(serializer.errors)

            return Response(data=serializer.data, status=status.HTTP_200_OK)

        else:
            response = get_response('REQUIRED_FIELDS')
            return Response(data=response['data'], status=response['status'])  

    def partial_update(self, request, pk=None): 
        instance = self.model_class.objects.get(**{self.lookup_fields: pk})

        if request.user != instance.user:
            raise PermissionDenied

        # FIX to prevent that user cant assign album to other user.
        info = request.data
        if 'user' in info:
            if info['user'] != request.user.pk:
                raise PermissionDenied

        serializer = self.serializer_class(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

class HashtagViewSet(viewsets.ModelViewSet):
    http_method_names = ['retrieve', 'get','create','post']
    model_class = Hashtag
    queryset = Hashtag.objects.all()
    serializer_class = HashtagSerializer
    paginate_by = 20
    lookup_fields = ('identifier')

    def get_permissions(self):
        self.permission_classes = [IsAuthenticated, ]
        return super(HashtagViewSet, self).get_permissions()

    def get_queryset(self):
        """
        This view should return a list pictures. If return the logged user, return all pictures. 
        If return another user, only public pictures.
        """
        query = self.request.query_params
        queryset = self.queryset
        if 'text' in query and len(query['text']) >= 3:
            queryset = self.queryset.filter(name__icontains=query['text'])[:20]
        else:
            queryset = Hashtag.objects.none()
        return queryset


class UserTagViewSet(viewsets.ModelViewSet):
    http_method_names = ['retrieve', 'get','create','post', 'patch', 'delete']
    model_class = Usertag
    queryset = Usertag.objects.all()
    serializer_class = UsertagSerializer
    paginate_by = 20
    lookup_fields = ('identifier')

    def get_permissions(self):
        self.permission_classes = [IsAuthenticated, ]
        return super(UserTagViewSet, self).get_permissions()

    def get_queryset(self):
        """
        This view should return a list of usertags. 
        """
        query = self.request.query_params
        queryset = self.queryset
        picture = None

        if 'picture' in query:
            picture = Picture.objects.get(identifier=str(query['picture']))
            if picture.user == self.request.user:
                queryset = self.queryset.filter(picture__identifier=str(query['picture']))
            else:
                queryset = self.queryset.filter(picture__identifier=str(query['picture']), hide=False)

        else:
            queryset = Usertag.objects.none()
        return queryset.filter(active=True).exclude(user__delete_account=True)

    def create(self, request, format=None):
        '''
        On this view you can:
            - Add new comment
            
        We receive:
        {
            "picture":"picture_identifier",
            "user":"user_identifier",
        }
        '''

        if not request.user:
            response = get_response('USER_NOT_EXISTS')
            return Response(data=response['data'], status=response['status'])

        info = request.data
        print(info)
        if ('picture' in info and 'top' in info and 'left' in info and 'status' in info) and ('user' in info or 'non_user' in info):
            if 'user' in info:
                try:
                    user = User.objects.get(identifier=info['user'])
                    # If user has been deleted, you can't tag him.
                    if user.delete_account: 
                        response = get_response('DELETED_USER')
                        return Response(data=response['data'], status=response['status'])

                except User.DoesNotExist:
                    user = None
                    response = get_response('USER_NOT_EXISTS')
                    return Response(data=response['data'], status=response['status'])
            else:
                user = None

            try:
                picture = Picture.objects.get(identifier=info['picture'])
            except Picture.DoesNotExist:
                picture = None
                response = get_response('PICTURE_NOT_EXISTS')
                return Response(data=response['data'], status=response['status'])

            if 'user' in info:
                try:
                    usertag = Usertag.objects.get(picture=picture,user=user,active=True,hide=False) # Fixed for tag control. 
                except Usertag.DoesNotExist:
                    usertag = None
            else:
                try:
                    usertag = Usertag.objects.get(picture=picture,non_user=info['non_user'],active=True)
                except Usertag.DoesNotExist:
                    usertag = None

            if info['status'] == 'remove':
                if usertag is not None:
                    usertag.active=False
                    usertag.save()
                    response = get_response('USERTAG_REMOVED')
                    return Response(data=response['data'], status=response['status'])
                else:
                    response = get_response('USERTAG_NOT_EXISTS')
                    return Response(data=response['data'], status=response['status'])
            elif info['status'] == 'add':
                print(usertag)
                if usertag is not None:
                    response = get_response('USERTAG_ALREADY_EXISTS')
                    return Response(data=response['data'], status=response['status'])
                else:
                    
                    # Check if exists a Tag Control pending of validate.
                    try:
                        usertag = Usertag.objects.get(picture=picture,user=request.user,active=True,hide=True)
                        usertag.active= False # Remove the tag
                        usertag.save()
                    except Usertag.DoesNotExist:
                        print('NOt exists any tag hide for this user on this picture.')
                    
                    new_tag = Usertag()
                    if 'user' in info:
                        new_tag.user = user
                    else:
                        new_tag.non_user = info['non_user']
                    new_tag.picture = picture
                    new_tag.owner = request.user
                    new_tag.top = info['top']
                    new_tag.left = info['left']

                    # Check if conections exists between users
                    check_user_follow_to = UserFollow.objects.filter(user=new_tag.user, follow_to=new_tag.owner, active=True).count()
                    check_follow_to_user = UserFollow.objects.filter(user=new_tag.owner, follow_to=new_tag.user, active=True).count()

                    if 'user' in info:
                        if (check_user_follow_to < 1 or check_follow_to_user < 1) and new_tag.user != new_tag.owner:
                            print('NO CONNECTION')
                            new_tag.hide = True
                        if new_tag.user.tag_control == True and request.user != new_tag.user: #Added control to tag myself and ignore my tagcontrol.
                            print('--- TAG CONTROL ENABLED ---')
                            new_tag.hide = True

                    else:
                        new_tag.hide = False

                    new_tag.save()

                    serializer = UsertagSerializer(new_tag, partial=False, many=False, context={'user': self.request.user, "request": self.request})

                    # Send notifications
                    if new_tag.user != request.user:
                        if user:
                            # Fixed for tags without user. 
                            # Notify user who's tagged
                            if new_tag.hide:
                                # Notify when not is connection
                                send_notification_tag(user,picture.identifier,'ENABLE_USER_TAG')
                            '''
                            else:
                                send_notification_tag(user,picture.identifier,'SOMEONE-TAGGED-YOU')
                            '''
                            
                    # Notify picture owner
                    if picture.user != request.user:
                        send_notification_tag(picture.user,picture.identifier,'SOMEONE-TAGGED-YOUR-PICTURE')
                    
                    return Response(data=serializer.data, status=status.HTTP_200_OK)
            else: 
                response = get_response('UNAVAILABLE_STATUS')
                return Response(data=response['data'], status=response['status'])

        else:
            response = get_response('REQUIRED_FIELDS')
            return Response(data=response['data'], status=response['status'])



class TimelineViewSet(viewsets.ModelViewSet):
    http_method_names = ['get',]
    model_class = MediaItem
    queryset = MediaItem.objects.all()
    serializer_class = MediaItemSerializer
    paginate_by = 20
 
    def get_serializer_context(self):
        return {'request': self.request}

    def get_permissions(self):
        self.permission_classes = [IsAuthenticated, ]
        return super(TimelineViewSet, self).get_permissions()

    def get_queryset(self):
        """
        This view should return a list MediaItems (pictures or albums) ordered by newer. 
        """
        
        # The timeline must show my following users items.
        followings = self.request.user.list_following
        user = self.request.user
        query = self.request.query_params
        queryset = self.queryset.filter(
            Q(album__user__pk__in=followings)
            | Q(picture__user__pk__in=followings)
            | Q(album__user=user)
            | Q(picture__user=user)
        ).exclude(
            Q(picture__hide=True)
            | Q(picture__public=False)
            | Q(album__private=True)
        ).order_by('-created_at')

        if queryset.count() == 0 and user.num_following == 0:
            # We show to a user without followings a random list of public items
            queryset = self.queryset.filter(
                Q(album__private=False) 
                | Q(picture__public=True)
                ).exclude(album__user__delete_account=True, picture__user__delete_account=True).order_by('?')[:20] # Exclude deleted users

        return queryset


class ReportItemViewSet(viewsets.ModelViewSet):
    http_method_names = ['create','post']
    model_class = ReportItem
    queryset = ReportItem.objects.all()
    serializer_class = ReportItemSerializer
    paginate_by = 20
    lookup_fields = ('identifier')

    def get_permissions(self):
        self.permission_classes = [IsAuthenticated, ]
        return super(ReportItemViewSet, self).get_permissions()

    def create(self, request, format=None):
        '''
        On this view you can:
            - Add new report comment
            
        We receive:
        {
            "picture":"picture_identifier",
            "comment":"text message",
        }
        '''

        if not request.user:
            response = get_response('USER_NOT_EXISTS')
            return Response(data=response['data'], status=response['status'])

        
        info = request.data

        if 'comment' in info and 'picture' in info:
            try:
                picture = Picture.objects.get(identifier=info['picture'])
            except Picture.DoesNotExist:
                picture = None
                response = get_response('PICTURE_NOT_EXISTS')
                return Response(data=response['data'], status=response['status'])

            try:
                userreport = ReportItem.objects.get(picture=picture,user=request.user)
            except ReportItem.DoesNotExist:
                userreport = None

            if userreport is not None:
                response = get_response('USERREPORT_ALREADY_EXISTS')
                return Response(data=response['data'], status=response['status'])
            else:
                new_report = ReportItem()
                new_report.user = request.user
                new_report.picture = picture
                new_report.comment = info['comment']
                new_report.save()
                serializer = ReportItemSerializer(new_report, partial=False, many=False, context={'user': self.request.user, "request": self.request})
                return Response(data=serializer.data, status=status.HTTP_200_OK)


        else:
            response = get_response('REQUIRED_FIELDS')
            return Response(data=response['data'], status=response['status'])




class GetAddress(APIView):

    def get(self, request, format=None):
        received = request.data
        query = request.query_params

        if 'lat' in query.keys() and 'lon' in query.keys():
            lat = query['lat']
            lon = query['lon']
            address = get_full_address(lat,lon)
            data = {"address":address}
            response_status = status.HTTP_200_OK

        elif 'address' in query.keys():
            # do stuff to get google suggestions
            data = get_address_google(query['address'])
            response_status = status.HTTP_200_OK
        elif 'place_id' in query.keys():
            # do stuff to get google suggestions
            data = get_address_place_id(query['place_id'])
            response_status = status.HTTP_200_OK   
        else:
            response = get_response('REQUIRED_FIELDS')
            data = response['data']
            response_status = response['status']
        
        return Response(data=data, status=response_status)

class AcceptUserTag(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        '''
            We accept or reject a tag in this endpoint.
            - picture
            - status accept / reject
        '''
        info = request.data

        if 'picture' in info and 'status' in info:

            try:
                picture = Picture.objects.get(identifier=info['picture'])
            except Picture.DoesNotExist:
                picture = None
                response = get_response('PICTURE_NOT_EXISTS')
                return Response(data=response['data'], status=response['status'])

            userTag = Usertag.objects.filter(picture=picture, user=request.user, hide=True, active=True)

            if info['status'] == 'accept':
                hide = False
                active = True
            else:
                hide = True
                active = False

            for t in userTag:
                t.hide = hide
                t.active = active
                t.save()

            response = get_response('USERTAG_ENABLED')
            return Response(data=response['data'], status=response['status'])

        else:
            response = get_response('REQUIRED_FIELDS')
            data = response['data']
            response_status = response['status']







