from django.apps import AppConfig


class NotificationsConfig(AppConfig):
    name = 'facebusters_project.notifications'
    verbose_name = "Notifications"
