from django.apps import AppConfig


class DashboardConfig(AppConfig):
    name = 'facebusters_project.dashboard'
    verbose_name = "Dashboard"
