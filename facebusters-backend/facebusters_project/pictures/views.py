from django.shortcuts import render
from facebusters_project.pictures.models import Picture

# Create your views here.

def picture_detail(request, identifier):

    template = 'pictures/detail.html'
    params = {}
    try:
        picture = Picture.objects.get(identifier=identifier)
    except Picture.DoesNotExist:
        picture = None
    params['picture'] = picture
    print('ssssssss')
    print(picture)
    context={
      'object':picture
    }

    return render(request, template, {'picture': picture})


def picture_social(request, identifier):

    template = 'pictures/social.html'
    try:
        picture = Picture.objects.get(identifier=identifier)
    except Picture.DoesNotExist:
        picture = None
    print('ssssssss')
    print(picture)

    if picture:
        print(picture.image.url)
    
    context={
      'object': picture
    }

    return render(request, template, {'picture': picture})


