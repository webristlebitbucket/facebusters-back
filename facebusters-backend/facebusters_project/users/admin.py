from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from .models import User, UserFollow
from easy_thumbnails.files import get_thumbnailer


class MyUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User

class MyUserCreationForm(UserCreationForm):

    error_message = UserCreationForm.error_messages.update({
        'duplicate_username': 'This username has already been taken.'
    })

    class Meta(UserCreationForm.Meta):
        model = User

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])

@admin.register(User)
class MyUserAdmin(AuthUserAdmin):
    form = MyUserChangeForm
    add_form = MyUserCreationForm

    def avatar_url(self, obj):
        if obj.avatar:
            try:
                return u'<center><img src="{0}" class="avatar_list"/></center>'.format(get_thumbnailer(obj.avatar)['avatar_mini'].url,)
            except Exception as error:
                print(error)
        return None

    avatar_url.short_description = 'Avatar'
    avatar_url.allow_tags = True


    def view_followers(self, obj):
        label = u'<a href=/admin/users/userfollow/?follow_to={0}>Followers</a>'.format(obj.pk,)
        return label
    view_followers.short_description = 'View Followers'
    view_followers.allow_tags = True

    def view_following(self, obj):
        label = u'<a href=/admin/users/userfollow/?user={0}>Following</a>'.format(obj.pk,)
        return label
    view_following.short_description = 'View Followers'
    view_following.allow_tags = True

    def view_albums(self, obj):
        label = u'<a href=/admin/pictures/album/?user={0}>Albums</a>'.format(obj.pk,)
        return label
    view_albums.short_description = 'View Followers'
    view_albums.allow_tags = True

    def view_pictures(self, obj):
        label = u'<a href=/admin/pictures/picture/?user={0}>Pictures</a>'.format(obj.pk,)
        return label
    view_pictures.short_description = 'View Pictures'
    view_pictures.allow_tags = True

    readonly_fields = (
            'avatar_url','view_following', 'view_followers', 'view_pictures', 'view_albums')


    fieldsets = (
        ('User Profile', {'fields': ('avatar','delete_account', 'language')},),
    ) + AuthUserAdmin.fieldsets
    list_display = ('username','avatar_url','first_name', 'last_name', 'email', 'social_provider', 'social_provider_data',
        'is_active','view_following', 'view_followers', 'view_pictures',
        'view_albums', 'num_followers','num_following','num_tagged_in','num_albums','num_pictures','identifier')
    search_fields = ['username','first_name', 'last_name']


@admin.register(UserFollow)
class UserFollowAdmin(admin.ModelAdmin):
    list_display = ('user', 'follow_to','active', 'fb_follow')
    search_fields = ['user', 'name','created_at','updated_at']
