# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2019-01-11 09:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0019_auto_20181219_1851'),
    ]

    operations = [
        migrations.AddField(
            model_name='userfollow',
            name='fb_follow',
            field=models.BooleanField(default=False),
        ),
    ]
