# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2019-01-11 10:09
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0020_userfollow_fb_follow'),
        ('users', '0020_merge_20181219_1902'),
    ]

    operations = [
    ]
