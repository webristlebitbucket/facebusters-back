from rest_framework import status
from .code_responses_ok import responses_ok
from .code_responses_error import responses_nok


def get_response(code):
    try:
        return responses_ok[code]
    except:
        try:
            return responses_nok[code]
        except:
            return  {
                'data': {
                'code': 44444,
                'message': 'Response Does Not Exists',
                'key': 'WRONG_RESPONSE'
                },
                'status': status.HTTP_400_BAD_REQUEST
            }


            responses = {
                400001: {
                    'data': {
                        'code': 400001,
                        'message': _('Fields missing'),
                        'key': 'MISSING_FIELDS'
                        },
                    'status': status.HTTP_400_BAD_REQUEST
                },
                'RESET_PASSWORD_OK': {
                    'data': {
                        'code': 200001,
                        'message': _('Email sended'),
                        'key': 'RESET_PASSWORD_OK'
                        },
                    'status': status.HTTP_200_OK
                }
            }
